<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="java.util.List" import="jp.keronos.dto.CourseDto"
	import="jp.keronos.dto.CategoryDto" import="jp.keronos.dto.HistoryDto"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>コース一覧</title>
<script type="text/javascript">
<!--
	function deleteConfirm(channelName) {
		return window.confirm("「" + courseName + "」コースを本当に削除しますか？");
	}
// -->
</script>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>

		<c:if test="${ empty user }">
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<h1>ログインしてください</h1>
				</div>
				<div class="col-sm-2"></div>
			</div>
		</c:if>
		<c:if test="${ not empty user }">
			<div class="row">
				<div class="col-12">

					<p class="h4 mt-3 p-3 bg-light text-info rounded">
						<c:out value="${ courseDto.categoryName }" />
					</p>
					<table class="table table-striped">
						<thead>
							<tr>
								<th scope="col">コース名</th>
								<th scope="col">コース概要</th>
								<th scope="col">学習時間目安</th>
								<th scope="col">プラン</th>
								<th scope="col">全カリキュラム数</th>
								<th scope="col">履修済み</th>
							</tr>
						</thead>
						<c:if test="${ not empty user }">
							<tbody>
								<%
									List<CourseDto> courseList = (List<CourseDto>) request.getAttribute("courseList");
								List<CourseDto> courseCountList = (List<CourseDto>) request.getAttribute("courseCountList");
								//Map<Integer, List<CourseDto>> courseByCategoryIdList = (Map<Integer, List<CourseDto>>)request.getAttribute("courseByCategoryIdList");
								List<Integer> historyCountList = (List<Integer>) request.getAttribute("historyCountList");

								//int categoryId = categoryList.get(x).getCategoryId();
								//int courseId = courseList.get(x).getCourseId();

								for (int i = 0; i < courseList.size(); i++) {
									int courseId = courseList.get(i).getCourseId();
									//List<CourseDto> courseList = courseByCategoryIdList.get(categoryList.get(x).getCategoryId());
									String courseName = courseList.get(i).getCourseName();
									String courseOverview = courseList.get(i).getCourseOverview();
									int studyTime = courseList.get(i).getStudyTime();
									String trainCourse = courseList.get(i).getTrainCourse();
									int curriculumCount = courseCountList.get(i).getCurriculumCount();
									int historyCount = historyCountList.get(i);
								%>
								<tr>
									<td><a href="list-curriculum?courseId=<%=courseId%>"><%=courseName%></a></td>
									<td><%=courseOverview%></td>
									<td><%=studyTime%></td>
									<td><%=trainCourse%></td>
									<td><%=curriculumCount%></td>
									<td><%=historyCount%></td>
								</tr>
								<%
									}
								%>
							</tbody>
						</c:if>
					</table>
				</div>
			</div>
			<br> 
			<br>
		</c:if>
		<c:if test="${ not empty user }">
			<div class="mx-auto">
				<div class="float-right">
					<a href="list-message"
						class="btn btn-outline-secondary btn-sm ml-2">お問い合わせはこちら</a>
				</div>
			</div>
		</c:if>
			<div class="mx-auto">
				<div class="float-left">
			<form action="list-category">
				<button class="btn btn-success btn-sm">戻る</button>
			</form>
	</div>
	</div>
</body>
</html>


