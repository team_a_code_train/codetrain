<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>運用担当者一覧</title>
<script type="text/javascript">
<!--
function deleteConfirm(firstName) {
	return window.confirm("本当に削除しますか？");
}
//-->

</script>
</head>
<body>
	<%@ include file="operator-navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">運用担当者一覧</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">＃</th>
							<th scope="col">氏名</th>
							<th scope="col">メールアドレス</th>
							<th scope="col">権限</th>
							<th scope="col"><c:if
									test="${operator.operatorDivision eq 'ab' or operator.operatorDivision eq 'ac'}">
									<a href="form-operator" class="btn btn-primary btn-sm">登録</a>
								</c:if></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${ list }" var="dto" varStatus="status">
							<tr>
								<th scope="row" style="height: 50px"><c:out
										value="${ status.count }" /></th>
								<td><span style="white-space: pre-wrap"><c:out
											value="${ dto.lastName }${dto.firstName}" /></span></td>
								<td><span style="white-space: pre-wrap"><c:out
											value="${ dto.email }" /></span></td>

								<c:choose>
									<c:when test="${dto.operatorDivision eq 'ab' }">
										<td><c:out value="運用"></c:out></td>
									</c:when>
									<c:when test="${dto.operatorDivision eq 'ac' }">
										<td><c:out value="全権"></c:out></td>
									</c:when>
									<c:otherwise>
										<td><c:out value="なし"></c:out></td>
									</c:otherwise>
								</c:choose>
								<c:if test="${ operator.operatorDivision eq 'ab' or operator.operatorDivision eq 'ac'}">
									<td>
										<form
											action="view-operator?operatorListId=${dto.operatorId} }"
											method="get">
											<input type="hidden" name="operatorListId"
												value="${ dto.operatorId }">
											<button type="submit" class="btn btn-secondary btn-sm">編集</button>
										</form>
									</td>
									<td>
										<form action="delete-operator" method="post">
											<input type="hidden" name="operatorId"
												value="${ dto.operatorId }"> <input type="hidden"
												name="updateNumber" value="${ dto.updateNumber }">
											<button type="submit" class="btn btn-danger btn-sm"
												onclick="return deleteConfirm('${ dto.firstName }')">削除</button>
										</form>
									</td>
								</c:if>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
<body>
	<div class="row">
		<div class="col text-center">
			<form action="operator-menu">
				<button class="btn btn-success btn-sm">戻る</button>
			</form>
		</div>
	</div>
</body>
</html>