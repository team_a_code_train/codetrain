<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<meta charset="UTF-8">
<title>パスワード変更画面</title>
</head>
<body>
<%@ include file="operator-navbar.jsp"%>
<div class="container">
<div align="center">
	<c:forEach items="${ errorMessageList }" var="errorMessage">
		<div class="row">
			<div class="mx-auto text-danger">
				&nbsp;
				<c:out value="${ errorMessage }" />
			</div>
		</div>
	</c:forEach>
</div>
	<div class="row">
		<div class="col-12">
			<form action="change-operator-password" method="post">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">パスワード変更</p>
					<div class="form-group row">
					<label class="col-form-label col-5" for="operatorLoginPassword">現在のパスワード：</label>
						<div class="col-7">
							<input type="password" name="operatorLoginPassword" class="form-control form-control-sm">
						</div>
					<label class="col-form-label col-5" for="operatorNewPassword">新しいパスワード：</label>
						<div class="col-7">
							<input type="password" name="operatorNewPassword" class="form-control form-control-sm">
						</div>
					<label class="col-form-label col-5" for="operatorCheckPassword">パスワード(確認用)：</label>
						<div class="col-7">
							<input type="password" name="operatorCheckPassword" class="form-control form-control-sm">
						</div>
					</div>
				<div class="form-group row">
					<div class="mx-auto">
						<button type="submit" class="btn btn-primary">パスワード変更</button>
						<button type="button" class="btn btn-success" id="back">
							<a href="operator-menu"><font color="white">戻る</font></a>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>