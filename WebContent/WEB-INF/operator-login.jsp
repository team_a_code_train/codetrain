<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<meta charset="UTF-8">
<title>運用管理者ログイン画面</title>
</head>
<body>
<form id="operatorLogin" action="operator-login" method="post" class="form-inline">
<div class="container">
	<div class="row">
		<div class="mx-auto text-danger">
			<c:out value="${ Message }" />
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<p class="h4 mt-3 p-3 bg-light text-info rounded">運用管理ログイン</p>
				<div class="form-group row">
					<label class="col-form-label col-5" for="operatorLoginId">ログインID：</label>
						<div class="col-3">
							<input type="email" name="operatorLoginId" class="form-control form-control-sm"
							 placeholder="メールアドレス" maxlength="30">
						</div>
					<label class="col-form-label col-5" for="operatorLoginPassword">パスワード：</label>
						<div class="col-3">
							<input type="password" name="operatorLoginPassword" class="form-control form-control-sm"
							 placeholder="パスワード" maxlength="30">
						</div>
				</div>
			<div class="form-group row">
				<div class="mx-auto">
					<button type="submit" class="btn btn-dark" id="corporation-login">運用ログイン</button>
				</div>
				<div class="float-right">
					<button type="button" class="btn btn-primary" id="back">
						<a href="list-category"><font color="white">利用者</font></a>
					</button>
					<button type="button" class="btn btn-success" id="back">
						<a href="corporation-login"><font color="white">連絡窓口</font></a>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
</body>
</html>