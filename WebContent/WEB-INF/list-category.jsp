<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>カテゴリ一覧</title>
<script type="text/javascript">
<!--
	function deleteConfirm(channelName) {
		return window.confirm("「" + channelName + "」チャンネルを本当に削除しますか？");
	}
// -->
</script>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>

		<div class="row">
			<div class="col-sm-8">
				<c:if test="${ empty user }">
					<h1>ログインしてください</h1>
				</c:if>
			</div>
			<div class="col-sm-2"></div>
		</div>
		<div class="row">
			<div class="col-12">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">カテゴリ一覧</p>
				<c:if test="${ not empty user }">
					<table class="table table-striped">
						<thead>
							<tr>
								<th scope="col">カテゴリ</th>
								<th scope="col">概要</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${ categoryList }" var="categoryDto"
								varStatus="status">
								<tr>
									<td><a
										href="list-course?categoryId=${ categoryDto.categoryId }">${ categoryDto.categoryName }</a></td>
									<td>${ categoryDto.categoryOverview }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
		<br>
		<br>
				<c:if test="${ not empty user }">
				<div class="mx-auto">
				<div class="float-right">
					<a href="list-message"
						class="btn btn-outline-secondary btn-sm ml-2">お問い合わせはこちら</a>
						</div>
			</div>
		</c:if>
	</div>
</body>

</html>