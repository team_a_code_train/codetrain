<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.util.List"
import="jp.keronos.dto.CourseDto" import="jp.keronos.dto.HistoryDto" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>法人利用者詳細確認画面</title>
<link href="css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
<%@ include file="corporation-navbar.jsp"%>
	<div class="row">
		<div class="col-12">
			<p class="h4 mt-3 p-3 bg-light text-info rounded">利用者詳細</p>
			<p class="h6 mt-3 p-4 bg-light ">氏名：<c:out value="${ userDto.lastName }  ${ userDto.firstName }" /></p>
			<p class="h6 mt-3 p-4 bg-light ">スキル：<c:out value="${ userDto.skill }" /></p>
			<table class="table table-striped">
				<tbody>
					<tr>
						<td>コース名</td>
						<td>全カリキュラム数</td>
						<td>履修済みカリキュラム数</td>
					</tr>
					<%
						List<CourseDto> courseList = (List<CourseDto>)request.getAttribute("courseList");
						List<CourseDto> courseCountList = (List<CourseDto>)request.getAttribute("courseCountList");
						List<Integer> historyCountList = (List<Integer>)request.getAttribute("historyCountList");

						for(int i = 0; i < courseList.size(); i++) {
							String courseName = courseList.get(i).getCourseName();
							int curriculumCount = courseCountList.get(i).getCurriculumCount();
							int historyCount = historyCountList.get(i);
					%>
					<tr>
						<td><%= courseName %></td>
						<td><%= curriculumCount %></td>
						<td><%= historyCount %></td>
					</tr>
					<% } %>
				</tbody>
			</table>
				<div class="form-group row">
					<div class="mx-auto">
						<button type="button" class="btn btn-success" id="back">
							<a href="list-user"><font color="white">戻る</font></a>
						</button>
					</div>
				</div>
		</div>
	</div>
</body>
</html>