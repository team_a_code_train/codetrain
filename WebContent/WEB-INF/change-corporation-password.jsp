<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<meta charset="UTF-8">
<title>パスワード変更画面</title>
</head>
<body>
<%@ include file="corporation-navbar.jsp"%>
<div class="container">
<div align="center">
	<c:forEach items="${ errorMessageList }" var="errorMessage">
		<div class="row">
			<div class="mx-auto text-danger">
				&nbsp;
				<c:out value="${ errorMessage }" />
			</div>
		</div>
	</c:forEach>
</div>
	<div class="row">
		<div class="col-12">
			<form action="change-corporation-password" method="post">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">パスワード変更</p>
					<div class="form-group row">
					<label class="col-form-label col-5" for="corporationLoginPassword">現在のパスワード：</label>
						<div class="col-7">
							<input type="password" name="corporationLoginPassword"
								class="form-control form-control-sm" maxlength="20">
						</div>
					<label class="col-form-label col-5" for="corporationNewPassword">新しいパスワード：</label>
						<div class="col-7">
							<input type="password" name="corporationNewPassword"
								class="form-control form-control-sm" maxlength="20">
						</div>
					<label class="col-form-label col-5" for="corporationCheckPassword">パスワード(確認用)：</label>
						<div class="col-7">
							<input type="password" name="corporationCheckPassword"
								class="form-control form-control-sm" maxlength="20">
						</div>
					</div>
				<div class="form-group row">
					<div class="mx-auto">
						<button type="submit" class="btn btn-primary">パスワード変更</button>
						<button type="button" class="btn btn-success" id="back">
							<a href="list-user"><font color="white">戻る</font></a>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>