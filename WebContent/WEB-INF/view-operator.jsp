<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out
		value="${ not empty isAdd ? '運営担当者登録' : '運営担当者編集'}" /></title>

</head>
<body>
	<div class="container">
		<c:forEach items="${ errorMessageList }" var="errorMessage">
			<div class="row">
				<div class="mx-auto text-danger">
					&nbsp;
					<c:out value="${ errorMessage }" />
				</div>
			</div>
		</c:forEach>
		<table class="row">
			<div class="col-10">
				<form
					action="${ not empty isAdd ? 'add-operator' : 'edit-operator' }"
					method="post">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">
						<c:out value="${ not empty isAdd ? '運営担当者登録' : '運営担当者編集'}" />
					</p>

					<div class="form-group row">
						<input type="hidden" name="operatorId"
							value="${ data.operatorId }" /> <input type="hidden"
							name="updateNumber" value="${ data.updateNumber }" />
					</div>

					<div class="container">
						<tr class="form-group row">
							<label class="col-form-label col-2" for="lastName">姓</label>
							<div class="col-md-8">
								<input type="text" id="lastName" name="lastName"
									class="form-control" maxlength="30" value="${ data.lastName }" />
							</div>
						</tr>
					</div>

					<div class="container">
						<tr class="form-group row">
							<label class="col-form-label col-2" for="firstName">名</label>
							<div class="col-md-8">
								<input type="text" id="firstName" name="firstName"
									class="form-control" maxlength="30" value="${ data.firstName }" />
							</div>
						</tr>
					</div>
					<div class="container">
						<tr class="form-group row">
							<label class="col-form-label col-2" for="email">メールアドレス</label>
							<div class="col-md-8">
								<input type="email" id="email" name="email" class="form-control"
									maxlength="200" rows="4" value="${ data.email}" />
							</div>
						</tr>
					</div>



					<div class="container">
						<label class="col-form-label col-2" for="operatorDivision">権限</label>
						<div class="col-10">
							<select name="operatorDivision">
								<option value="aa" name="operatorDivision">なし</option>
								<option value="ab" name="operatorDivision">運用</option>
								<option value="ac" name="operatorDivision">全権</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<div class="mx-auto">
							<c:if test="${ empty isAdd }">
								<button type="reset" class="btn btn-secondary">リセット</button>
							</c:if>
							<button type="submit" class="btn btn-primary">${ not empty isAdd ? '登録' : '更新'}</button>
						</div>
					</div>
				</form>
			</div>
		</table>
	</div>
</body>
<body>
	<div class="row">
		<div class="col text-center">
			<form action="list-operator">
				<button class="btn btn-success btn-sm">戻る</button>
			</form>
		</div>
	</div>
</body>
</html>