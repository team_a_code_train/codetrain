<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="jp.keronos.dto.UserDto"
	import="jp.keronos.dto.CompanyDto" import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>企業一覧</title>
<script type="text/javascript">
<!--
	function deleteConfirm(company) {
		return window.confirm("本当に削除しますか？");
	}
// -->
</script>
</head>
<body>
	<%@ include file="operator-navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">企業一覧</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">会社ID</th>
							<th scope="col">会社名</th>
							<th scope="col">メールアドレス</th>
							<th scope="col">履修人数</th>
							<th scope="col">休止人数</th>
							<th scope="col"><c:choose>
								<c:when test="${ operator.operatorDivision eq 'ac' }">
									<a href="add-company" class="btn btn-primary btn-sm">登録</a>
								</c:when>
								<c:otherwise>
									<br>
								</c:otherwise>
							</c:choose></th>
							<th scope="col"><br></th>

						</tr>
					</thead>
					<tbody>
						<%
							List<CompanyDto> companyList = (List<CompanyDto>) request.getAttribute("companyList");
						List<UserDto> userCountList = (List<UserDto>) request.getAttribute("userCountList");

						for (int i = 0; i < companyList.size(); i++) {
							int companyId = companyList.get(i).getCompanyId();
							String companyName = companyList.get(i).getCompanyName();
							String email = companyList.get(i).getEmail();
							int userIdCount = userCountList.get(i).getUserIdCount();
							int status_flgCount = userCountList.get(i).getStatus_flgCount();
							int updateNumber = companyList.get(i).getUpdateNumber();

							int userCount = userIdCount - status_flgCount;
						%>
						<tr>
							<th scope="row" style="height: 50px"><%=i + 1%></th>
							<td><%=companyId%></td>
							<td><a href="list-corporation-user?companyId=<%=companyId%>"><%=companyName%></a></td>
							<td><span style="white-space: pre-wrap"><%=email%></span></td>
							<td><%=userCount%></td>
							<td><%=status_flgCount%></td>
							<c:if test="${ operator.operatorDivision eq 'ac' }">
								<td>
									<form action="view-company?companyListId=<%=companyId%>"
										method="get">
										<input type="hidden" name="companyListId"
											value="<%=companyId%>">
										<button type="submit" class="btn btn-secondary btn-sm">編集</button>
									</form>
								</td>
								<td>
									<form action="delete-company" method="post">
										<input type="hidden" name="companyId" value="<%=companyId%>">
										<input type="hidden" name="updateNumber"
											value="<%=updateNumber%>">
										<button type="submit" class="btn btn-danger btn-sm"
											onclick="return deleteConfirm('<%=companyId%>')">削除</button>
									</form>
								</td>
							</c:if>
						</tr>
						<%
							}
						%>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
<body>
	<div class="row">
		<div class="col text-center">
			<form action="operator-menu">
				<button class="btn btn-success btn-sm">戻る</button>
			</form>
		</div>
	</div>
</body>
</html>