<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>法人利用者一覧</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link href="css/bootstrap.css" rel="stylesheet">
<script type="text/javascript">
<!--
function deleteConfirm(user) {
	return window.confirm("ユーザを本当に削除しますか？");
}
//-->
</script>
</head>
<body>
<%@ include file="corporation-navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">利用者一覧</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<td>#</td>
							<td>氏名</td>
							<td>状態</td>
							<td>凍結理由</td>
							<td>
								<!--削除ボタンスペース用-->
							</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${ list }" var="dto" varStatus="status">
							<c:choose>
							<c:when test="${ dto.del_flg eq false
								 && dto.companyId == corporationUser.companyId }">
							<tr>
								<td>
									<a href="list-user-detail?userId=${ dto.userId }">
										<c:out value="${ status.count }" />
									</a>
								</td>
								<td><c:out value="${ dto.lastName }  ${ dto.firstName }" /></td>
								<td><a href="edit-user?userId=${ dto.userId }">
									<c:choose>
											<c:when test="${ dto.status_flg eq false }">
												<button type="submit" class="btn btn-warning" id="add-user">
													<font color="black">受講中</font>
												</button>
											</c:when>
											<c:otherwise>
												<button type="submit" class="btn btn-secondary" id="add-user">
													<font color="white">休止中</font>
												</button>
											</c:otherwise>
									</c:choose>
									</a>
								</td>
								<td><c:out value="${ dto.reason }" /></td>
								<td>
									<form name="delete" action="delete-user" method="post">
										<button type="submit" class="btn btn-danger" id="delete-user"
											onclick="return deleteConfirm('${ dto.userId }')">削除</button>
											<input type="hidden" name="userId" value="${ dto.userId }">
											<input type="hidden" name="del_flg" value="${ dto.del_flg }">
											<input type="hidden" name="updateNumber" value="${ dto.updateNumber }">
									</form>
								</td>
							</tr>
							</c:when>
							</c:choose>
						</c:forEach>
					</tbody>
				</table>
					<div class="form-group row">
						<div class="mx-auto">
						<button type="button" class="btn btn-primary" id="add-user">
							<a href="add-user"><font color="white">利用者の新規登録</font></a>
						</button>
						<button type="button" class="btn btn-warning" id="invoice-month">
							<a href="invoice-month?companyId=${ corporationUser.companyId }">
								<font color="black">各月の請求明細確認</font>
							</a>
						</button>
						<button type="button" class="btn btn-success"
								id="change-corporation-password">
							<form action="change-corporation-password" method="post">
								<a href="change-corporation-password">
									<font color="white">パスワードの変更</font>
								</a>
							</form>
						</button>
						</div>
					</div>
			</div>
		</div>
	</div>
</body>
</html>