<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学習機能</title>
<%@ include file="header.jsp"%>
<!--<link href="css/bootstrap.css" rel="stylesheet">-->
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>

		<form id="history" action="add-history" method="post">
			<c:forEach items="${ studyList }" var="ListCurriculumDto">
			<div class="form-group row">
				<input type="hidden" name="courseId" value="${ ListCurriculumDto.courseId }" />
				<input type="hidden" name="categoryId" value="${ ListCurriculumDto.categoryId }" />
			</div>​
			<div class="row">
					<div class="col-12">
						<p class="h4 mt-3 p-3 bg-light text-info rounded">
							<c:out value="${ ListCurriculumDto.curriculum }" />
						</p>
						<label class="col-form-label col-2" for="study">テキスト</label>
						<div class="col-10">
							<textarea id="study" name="study" class="form-control" rows="15"><c:out
									value="${ ListCurriculumDto.text }" /></textarea>
						</div>
						​
					</div>
				</div>
				<div class="form-group row">
					<div class="mx-auto">
						<button type="submit" class="btn btn-primary">完了</button>
					</div>
				</div>
			</c:forEach>
		</form>
	</div>
</body>
</html>



