<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>凍結理由申請画面</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>
<%@ include file="corporation-navbar.jsp"%>
<div class="container">
	<c:forEach items="${ errorMessageList }" var="errorMessage">
		<div class="row">
			<div class="mx-auto text-danger">
				&nbsp;
				<c:out value="${ errorMessage }" />
			</div>
		</div>
	</c:forEach>
	<form action="edit-user" method="post">
	<div class="form-group row">
		<c:choose>
			<c:when test="${ userDto.status_flg eq false }">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">休止理由記入フォーム</p>
					<div class="col-9">
						<textarea id="reason" name="reason" style="resize: none;"
						 class="form-control" maxlength="200" rows="4"><c:out value="${ userDto.reason }" /></textarea>
					</div>
			</c:when>
		</c:choose>
	</div>
		<div class="form-group row">
			<div class="mx-auto">
				<button type="submit" class="btn btn-primary">更新</button>
				<button type="reset" class="btn btn-secondary">リセット</button>
			</div>
		</div>
			<input type="hidden" name="updateNumber" value="${ userDto.updateNumber }">
			<input type="hidden" name="userId" value="${ userDto.userId }">
	</form>
</div>
</body>
</html>