<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>請求月確認画面</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>
<%@ include file="corporation-navbar.jsp"%>
<div class="container">
	<c:forEach items="${ errorMessageList }" var="errorMessage">
		<div class="row">
			<div class="mx-auto text-danger">
				&nbsp;
				<c:out value="${ errorMessage }" />
			</div>
		</div>
	</c:forEach>
	<div class="form-group row">
	<div class="col-12">
	<p class="h4 mt-3 p-3 bg-light text-info rounded">請求月一覧</p>
		<hr>
		<table class="table table-striped">
			<tbody>
				<c:choose>
					<c:when test="${ not empty monthList }">
					<tr>
						<td>請求月</td>
					</tr>
					<c:forEach items="${ monthList }" var="dto" varStatus="status">
						<tr>
							<td>
								<form action="invoice-detail" method="post">
									<button type="submit" id="invoice-detail">
										${ dto.month }月
									</button>
									<input type="hidden" name="companyId" value="${ companyId }">
									<input type="hidden" name="month" value="${ dto.month }">
								</form>
							</td>
						</tr>
					</c:forEach>
					</c:when>
					<c:otherwise>
						<tr>
							<td>請求書がまだ発行されていません</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		<div class="form-group row">
			<div class="mx-auto">
				<button type="button" class="btn btn-success" id="back">
					<a href="list-user"><font color="white">戻る</font></a>
				</button>
			</div>
		</div>
	</div>
	</div>
</div>
</body>
</html>