<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="list-user">
		<img src="img/CodeTrain.png" height="80" width="150" /></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<div class="navbar-text small mr-3 text-danger">
			<c:out value="${ navbarMessage }" />
		</div>
		<div class="navbar-text small mr-3 text-secondary">
			<c:out value="${ not empty corporationUser.firstName ? corporationUser.lastName += ' '
				+= corporationUser.firstName += 'さん、こんにちは' : '' }" />
		</div>
		<form action="corporation-logout" method="post" class="form-inline">
			<button type="submit" class="btn btn-outline-danger btn-sm">ログアウト</button>
		</form>
		<div class="mx-auto">
			<div class="float-right">
				<a href="list-category" class="btn btn-outline-primary btn-sm ml-2">利用者</a>
				<a href="operator-login" class="btn btn-outline-dark btn-sm ml-2">運用管理</a>
			</div>
		</div>
	</div>
</nav>
