<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>企業アカウント一覧</title>
<script type="text/javascript">
<!--
	function deleteConfirm(firstName) {
		return window.confirm("アカウントを本当に削除しますか？");
	}
// -->
</script>
</head>
<body>
	<%@ include file="operator-navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">企業アカウント一覧</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">姓</th>
							<th scope="col">名</th>
							<th scope="col">会社ID</th>
							<th scope="col">会社名</th>
							<th scope="col">メールアドレス</th>

							<th scope="col"><c:choose>
									<c:when test="${ operator.operatorDivision eq 'ac' }">
										<a href="add-corporation-user"> <input type="hidden"
											name="companyId" value="${ dto.companyId }"> <input
											type="hidden" name="companyName" value="${ dto.companyName}">
											<button type="submit" class="btn btn-primary btn-sm">登録</button></a>
									</c:when>
									<c:otherwise>
										<br>
									</c:otherwise>
								</c:choose></th>
							<th scope="col"><br></th>

						</tr>
					</thead>
					<tbody>
						<c:forEach items="${ list }" var="dto" varStatus="status">
							<tr>
								<th scope="row" style="height: 50px"><c:out
										value="${ status.count }" /></th>
								<td><span style="white-space: pre-wrap"><c:out
											value="${ dto.lastName }" /></span></td>
								<td><span style="white-space: pre-wrap"><c:out
											value="${ dto.firstName }" /></span></td>
								<td><span style="white-space: pre-wrap"><c:out
											value="${ dto.companyId }" /></span></td>
								<td><a href="list-company?companyId=${ dto.companyId }">${ dto.companyName }</a></td>
								<td><span style="white-space: pre-wrap"><c:out
											value="${ dto.email }" /></span></td>


								<c:if test="${ operator.operatorDivision eq 'ac' }">
									<td><a href="view-corporation-user?userId=${ dto.userId }">
											<input type="hidden" name="companyId"
											value="${ dto.companyId }"> <input type="hidden"
											name="companyName" value="${ dto.companyName}">
											<button type="submit" class="btn btn-secondary btn-sm">編集</button>
									</a></td>
									<td>
										<form action="delete-corporation-user" method="post">
											<input type="hidden" name="userId" value="${ dto.userId }">
											<input type="hidden" name="updateNumber"
												value="${ dto.updateNumber }">
											<button type="submit" class="btn btn-danger btn-sm"
												onclick="return deleteConfirm('${ dto.firstName }')">削除</button>
										</form>
									</td>
								</c:if>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
<body>
	<div class="row">
		<div class="col text-center">
			<form action="list-company">
				<button class="btn btn-success btn-sm">戻る</button>
			</form>
		</div>
	</div>
</body>
</html>