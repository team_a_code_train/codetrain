<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>コース詳細</title>
<script type="text/javascript">
<!--
	function deleteConfirm(channelName) {
		return window.confirm("「" + courseName + "」コースを本当に削除しますか？");
	}
// -->
</script>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<c:forEach items="${ courseList }" var="CourseDto"
					varStatus="status">
					<p class="h5 mt-3 p-3 bg-light text-info rounded">
						カテゴリー【
						<c:out value="${ CourseDto.categoryName }" />
						】
					</p>
					<div class="container">
						<div class="col-md-7">
							<table class="h3 mt-3 p-3 bg-light text-info rounded text-dark">
								<tr>
									<td>コース【 <c:out value="${ CourseDto.courseName }" /> 】
									</td>
									<td><c:out value="${ CourseDto.trainCourse }" /></td>
									<td>目安時間 : <c:out value="${ CourseDto.studyTime }" /> 時間
									</td>
								</tr>
							</table>
						</div>
					</div>
					<br>
					<table>
						<tr>
							<td>コース概要: <c:out value="${ CourseDto.courseOverview }" /></td>
						</tr>
						<tr>
							<td>前提条件: <c:out value="${ CourseDto.precondition }" /></td>
						</tr>
						<tr>
							<td>ゴール: <c:out value="${ CourseDto.goal }" /></td>
						</tr>
					</table>
				</c:forEach>
			</div>
		</div>
		<br>
		<div class="container">
			<div class="col-md-7">
				<p class="h3 bg-light text-info rounded text-dark">カリキュラム</p>
				<ol class="h3 mt-3 p-3 bg-light text-inforounded text-dark">
					<c:forEach items="${ curriculumlist }" var="ListCurriculumDto"
						varStatus="status">
						<li><a
							href="study?curriculumId=${ ListCurriculumDto.curriculumId }" target="_blank">${ ListCurriculumDto.curriculum }</a></li>
					</c:forEach>
				</ol>
			</div>
		</div>
</body>
</html>