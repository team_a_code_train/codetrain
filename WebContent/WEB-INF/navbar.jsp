<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="index.jsp">
	<img src="img/CodeTrain.png" height="80" width="150" /></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<div class="navbar-text small mr-3 text-danger">
			<c:out value="${ navbarMessage }" />
		</div>
		<div class="navbar-text small mr-3 text-secondary">
			<c:out value="${ not empty user.lastName ? user.lastName += ' ' += user.firstName += 'さん、こんにちは' : '' }" />
		</div>
		<c:choose>
			<c:when test="${ empty user }">
				<form id="form-nav" action="login" method="post" class="form-inline">
					<input type="email" name="id" class="form-control form-control-sm" placeholder="メールアドレス">
					<input type="password" name="password" class="form-control form-control-sm" placeholder="パスワード">
					<input type="hidden" name="uri" value="${ requestScope.uri }">
					<button type="submit" class="btn btn-outline-primary btn-sm">ログイン</button>
				</form>
			</c:when>
			<c:otherwise>
				<form action="logout" method="post" class="form-inline">
					<button type="submit" class="btn btn-outline-danger btn-sm">ログアウト</button>
				</form>
				<a href="change-password" class="btn btn-outline-secondary btn-sm ml-2">パスワード変更</a>
			</c:otherwise>
		</c:choose>
		<div class="mx-auto">
			<div class="float-right">
				<a href="corporation-login" class="btn btn-outline-success btn-sm ml-2">連絡窓口</a>
				<a href="operator-login" class="btn btn-outline-dark btn-sm ml-2">運用管理</a>
			</div>
		</div>
	</div>
</nav>
