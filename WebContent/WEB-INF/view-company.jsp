<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="${ not empty isAdd ? '法人登録' : '法人編集'}" /></title>

</head>
<body>
	<div class="container">
		<c:forEach items="${ errorMessageList }" var="errorMessage">
			<div class="row">
				<div class="mx-auto text-danger">
					&nbsp;
					<c:out value="${ errorMessage }" />
				</div>
			</div>
		</c:forEach>
		<table class="row">
			<div class="col-10">
				<form action="${ not empty isAdd ? 'add-company' : 'edit-company' }"
					method="post">
					<p class="h4 mt-3 p-3 bg-light text-info rounded">
						<c:out value="${ not empty isAdd ? '法人登録' : '法人編集'}" />
					</p>

					<div class="form-group row">
						<input type="hidden" name="companyId" value="${ data.companyId }" />
						<input type="hidden" name="updateNumber"
							value="${ data.updateNumber }" />
					</div>
					<div class="container">
						<tr class="form-group row">
							<label class="col-form-label col-2" for="companyName">法人名</label>
							<div class="col-md-8">
								<input type="text" id="companyName" name="companyName"
									class="form-control" maxlength="30"
									value="${ data.companyName }" />
							</div>
						</tr>
					</div>
					<div class="container">
						<tr class="form-group row">
							<label class="col-form-label col-2" for="email">メールアドレス</label>
							<div class="col-md-8">
								<input type="email" id="email" name="email" class="form-control"
									maxlength="200" rows="4" value="${ data.email}" />
							</div>
						</tr>
					</div>
					<div class="form-group row">
						<div class="mx-auto">
							<c:if test="${ empty isAdd }">
								<button type="reset" class="btn btn-secondary">リセット</button>
							</c:if>
							<button type="submit" class="btn btn-primary">${ not empty isAdd ? '登録' : '更新'}</button>
						</div>
					</div>
				</form>
			</div>
		</table>
	</div>
</body>
<body>
	<div class="row">
		<div class="col text-center">
			<form action="list-company">
				<button class="btn btn-success btn-sm">戻る</button>
			</form>
		</div>
	</div>
</body>
</html>