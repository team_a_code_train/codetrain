<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>法人利用者追加画面</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>
<%@ include file="corporation-navbar.jsp"%>
<div class="container">
	<c:forEach items="${ errorMessageList }" var="errorMessage">
		<div class="row">
			<div class="mx-auto text-danger">
				&nbsp;
				<c:out value="${ errorMessage }" />
			</div>
		</div>
	</c:forEach>
	<form action="add-user" method="post">
	<div class="form-group row">
	<div class="col-12">
	<p class="h4 mt-3 p-3 bg-light text-info rounded">法人利用者追加フォーム</p>
		<table class="table table-striped">
			<tbody>
				<tr>
					<td>氏名：</td>
					<td><input type="text" name="lastName" class="form-control form-control-sm" placeholder="姓"></td>
					<td><input type="text" name="firstName" class="form-control form-control-sm" placeholder="名"></td>
				</tr>
				<tr>
					<td>メールアドレス：</td>
					<td><input type="email" name="email" class="form-control form-control-sm"
						placeholder="メールアドレス">
					</td>
				</tr>
			</tbody>
		</table>
		<div class="form-group row">
			<div class="mx-auto">
				<button type="submit" class="btn btn-primary btn-sm" id="add-user">
					<font color="white">追加</font>
				</button>
				<button type="reset" class="btn btn-secondary btn-sm">
					<font color="white">リセット</font>
				</button>
				<button type="button" class="btn btn-success btn-sm" id="back">
					<a href="list-user"><font color="white">戻る</font></a>
				</button>
			</div>
		</div>
	</div>
	</div>
	</form>
</div>
</body>
</html>