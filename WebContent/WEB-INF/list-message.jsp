<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>お問い合わせ</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>
​
		<div class="row">
			<div class="col-12">
			<form action="add-message" method="post">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">本サービスに関するお問い合わせ・返信</p>
					<div class="form-group row">
						<input type="hidden" name="userId" value="${ user.userId }" />
					</div>
					<c:set var="newLine" value="\r\n" />
					<div class="form-group row">
						<label class="col-form-label col-2" for="message">メッセージ</label>
						<div class="col-10">
							<textarea id="message" name="message" class="form-control"
								rows="15"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="mx-auto">
							<button type="submit" class="btn btn-secondary">送信</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">メッセージ履歴</p>
				<c:forEach items="${ messageList }" var="messageDto">
					<c:set value="${ messageDto.message }" var="message" />
					<div class="card mb-2">
						<div class="card-body">
							<span style="white-space: pre-wrap"><c:out
									escapeXml="false" value="${ message }" /></span>
							<footer class="blockquote-footer text-right">
								<c:out value="${ messageDto.date }" />
							</footer>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</body>
</html>