<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>運用管理メニュー</title>
	<script type="text/javascript">

	</script>
	<%--<!--  <%@ include file="header.jsp" -->--%>
</head>
<body>
<%@ include file="operator-navbar.jsp"%>
	<%--<!--  <%@ include file="navbar.jsp" -->--%>
	<body>
  <div class="row">
    <div class="col-md-6">
     <h1><p class="h4 mt-3 p-3 bg-light text-info rounded">運用管理メニュー画面</p></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-1">
   
    <table>
      <ul class="line-height: 2em">
      <%--リンクは後ほど記述 --%>
        <li class="h6 mt-3 p-3 bg-light text-info rounded"><a href="list-operator" target="_blank">運用担当者一覧</a></li>
        <li class="h6 mt-3 p-3 bg-light text-info rounded"><a href="list-company" target="_blank">法人一覧</a></li>
        <li class="h6 mt-3 p-3 bg-light text-info rounded"><a href="" target="_blank">テキスト作成・修正</a></li>
        <li class="h6 mt-3 p-3 bg-light text-info rounded"><a href="list-contact" target="_blank">利用者との連絡</a></li>
        <li class="h6 mt-3 p-3 bg-light text-info rounded"><a href="change-operator-password" target="_blank">パスワード変更</a></li>
      </ul>
      </table>
   
    </div>
  </div>

</body>