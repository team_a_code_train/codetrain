<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>連絡事項一覧</title>
<script type="text/javascript">
	
</script>
</head>
<body>
	<%@ include file="operator-navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<p class="h4 mt-3 p-3 bg-light text-info rounded">連絡事項一覧</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">日付</th>
							<th scope="col">会社名</th>
							<th scope="col">ユーザー名</th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${ list }" var="dto" varStatus="status">
							<tr>
								<td><span style="white-space: pre-wrap"><c:out
											value="${ dto.date }" /></span></td>
								<td><span style="white-space: pre-wrap"><c:out
											value="${ dto.companyName }" /></span></td>
											<td><span style="white-space: pre-wrap"><c:out
											value="${ dto.lastName }${dto.firstName}" /></span></td>
								<c:if test="${ operator.operatorDivision eq 'ab' || operator.operatorDivision eq 'ac'}">
									<td>
										<button type="submit" class="btn btn-info btn-sm">
											<a href="list-operator-message?userId=${ dto.userId }">
												<font color="black">返信</font>
											</a>
										</button>
									</td>
								</c:if>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
<body>
	<div class="row">
		<div class="col text-center">
			<form action="operator-menu">
				<button class="btn btn-success btn-sm">戻る</button>
			</form>
		</div>
	</div>
</body>
</html>