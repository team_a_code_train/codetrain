package jp.keronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.InvoiceDto;

public class InvoiceDao {

	protected Connection conn;

	public InvoiceDao(Connection conn) {
		this.conn = conn;
	}

	public List<InvoiceDto> selectAllMonthBycompanyId(int companyId) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" DATE_FORMAT(DATE, '%m') as MONTH");
		sb.append(" ,DATE");
		sb.append(" from INVOICE");
		sb.append(" where COMPANY_ID = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, companyId);

			List<InvoiceDto> list = new ArrayList<>();
			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			while (rs.next()) {
				InvoiceDto dto = new InvoiceDto();
				dto.setCompanyId(companyId);
				dto.setMonth(rs.getInt("MONTH"));
				dto.setDate(rs.getTimestamp("DATE"));
				list.add(dto);
				return list;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	public InvoiceDto findByCompanyIdAndDate(int companyId, Timestamp date) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" INVOICE_ID");
		sb.append(" ,COMPANY_NAME");
		sb.append(" ,COMPANY_ID");
		sb.append(" ,PRICE");
		sb.append(" ,DATE");
		sb.append(" from INVOICE");
		sb.append(" where COMPANY_ID = ?");
		sb.append(" and DATE = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, companyId);
			ps.setTimestamp(2, date);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				InvoiceDto dto = new InvoiceDto();
				dto.setInvoiceId(rs.getInt("INVOICE_ID"));
				dto.setCompanyName(rs.getString("COMPANY_NAME"));
				dto.setCompanyId(rs.getInt("COMPANY_ID"));
				dto.setPrice(rs.getInt("PRICE"));
				dto.setDate(rs.getTimestamp("DATE"));

				return dto;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}
}
