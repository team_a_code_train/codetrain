package jp.keronos.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.CategoryDto;

public class CategoryDao {
	/** コネクション ですうううう*/
	protected Connection conn;

	/**
	 * コンストラクタです
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public CategoryDao(Connection conn) {
        this.conn = conn;
    }

	/**
	 * チャンネル情報を取得する
	 * @return チャンネル情報リスト
	 * @throws SQLException SQL例外
	 */

	public List<CategoryDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          CATEGORY_ID");
		sb.append("         ,CATEGORY_NAME");
		sb.append("         ,CATEGORY_OVERVIEW");
		sb.append("     from CATEGORY");
		sb.append(" order by CATEGORY_ID");

		List<CategoryDto> list = new ArrayList<>();
    	// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());
			while (rs.next()) {
				CategoryDto dto = new CategoryDto();
				dto.setCategoryId(rs.getInt("CATEGORY_ID"));
				dto.setCategoryName(rs.getString("CATEGORY_NAME"));
				dto.setCategoryOverview(rs.getString("CATEGORY_OVERVIEW"));
				list.add(dto);
			}
			return list;
		}
	}
}

