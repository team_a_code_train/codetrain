package jp.keronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.keronos.dto.CourseDto;

/**
 * コーステーブルのDataAccessObject
 */
public class CourseListDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ コネクションをフィールドに設定する
	 *
	 * @param conn コネクション
	 */
	public CourseListDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * コース一覧情報リストを全件取得する コースIDで昇順にする
	 *
	 * @throws SQLException
	 */
	public List<CourseDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		// コース一覧情報リストを全件取得する
		sb.append("      COURSE.COURSE_ID");
		sb.append("     ,COURSE.COURSE_NAME");
		sb.append("     ,COURSE.COURSE_OVERVIEW");
		sb.append("     ,COURSE.PRECONDITION");
		sb.append("     ,COURSE.GOAL");
		sb.append("     ,COURSE.TRAIN_COURSE");
		sb.append("     ,COURSE.STUDY_TIME");
		sb.append("     ,COURSE.CURRICULUM_ID");
		sb.append("     ,COURSE.CATEGORY_ID");
		sb.append("     ,COURSE.VIEW_FLG");
		sb.append("     ,COURSE.UPDATE_NUMBER");
		// カテゴリIDに紐づけてカテゴリ名を取得する
		sb.append("     ,CATEGORY.CATEGORY_NAME");
		sb.append("          from COURSE");
		sb.append("	     inner join CATEGORY on COURSE.CATEGORY_ID = CATEGORY.CATEGORY_ID ");
		// コースIDで降順にする
		sb.append(" order by COURSE_ID");

		List<CourseDto> list = new ArrayList<>();

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				CourseDto dto = new CourseDto();
				dto.setCourseId(rs.getInt("COURSE_ID"));
				dto.setCourseName(rs.getString("COURSE_NAME"));
				dto.setCourseOverview(rs.getString("COURSE_OVERVIEW"));
				dto.setPrecondition(rs.getString("PRECONDITION"));
				dto.setGoal(rs.getString("GOAL"));
				dto.setTrainCourse(rs.getString("TRAIN_COURSE"));
				dto.setStudyTime(rs.getInt("STUDY_TIME"));
				dto.setCurriculumId(rs.getInt("CURRICULUM_ID"));
				dto.setCategoryId(rs.getInt("CATEGORY_ID"));
				dto.setViewFlg(rs.getInt("VIEW_FLG"));
				dto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				dto.setCategoryName(rs.getString("CATEGORY_NAME"));
				list.add(dto);
			}
		}
		return list;
	}

	public List<CourseDto> selectByCategoryId(int categoryId) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		// コース一覧情報リストを全件取得する
		sb.append("      COURSE.COURSE_ID");
		sb.append("     ,COURSE.COURSE_NAME");
		sb.append("     ,COURSE.COURSE_OVERVIEW");
		sb.append("     ,COURSE.PRECONDITION");
		sb.append("     ,COURSE.GOAL");
		sb.append("     ,COURSE.TRAIN_COURSE");
		sb.append("     ,COURSE.STUDY_TIME");
		sb.append("     ,COURSE.CURRICULUM_ID");
		sb.append("     ,COURSE.CATEGORY_ID");
		sb.append("     ,COURSE.VIEW_FLG");
		sb.append("     ,COURSE.UPDATE_NUMBER");
		// カテゴリIDに紐づけてカテゴリ名を取得する
		sb.append("     ,CATEGORY.CATEGORY_NAME");
		sb.append("          from COURSE");
		sb.append("	     inner join CATEGORY on COURSE.CATEGORY_ID = CATEGORY.CATEGORY_ID ");
		// コースIDで降順にする
		sb.append(" where COURSE.CATEGORY_ID = ?");
		sb.append(" order by COURSE_ID");

		List<CourseDto> list = new ArrayList<>();

		// ステートメントオブジェクトを作成
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, categoryId);

			// SQL文を実行
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				CourseDto dto = new CourseDto();
				dto.setCourseId(rs.getInt("COURSE_ID"));
				dto.setCourseName(rs.getString("COURSE_NAME"));
				dto.setCourseOverview(rs.getString("COURSE_OVERVIEW"));
				dto.setPrecondition(rs.getString("PRECONDITION"));
				dto.setGoal(rs.getString("GOAL"));
				dto.setTrainCourse(rs.getString("TRAIN_COURSE"));
				dto.setStudyTime(rs.getInt("STUDY_TIME"));
				dto.setCurriculumId(rs.getInt("CURRICULUM_ID"));
				dto.setCategoryId(rs.getInt("CATEGORY_ID"));
				dto.setViewFlg(rs.getInt("VIEW_FLG"));
				dto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				dto.setCategoryName(rs.getString("CATEGORY_NAME"));
				list.add(dto);
			}
		}
		return list;
	}
	
	public List<CourseDto> selectByCourseId(int courseId) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		// コース一覧情報リストを全件取得する
		sb.append("      COURSE.COURSE_ID");
		sb.append("     ,COURSE.COURSE_NAME");
		sb.append("     ,COURSE.COURSE_OVERVIEW");
		sb.append("     ,COURSE.PRECONDITION");
		sb.append("     ,COURSE.GOAL");
		sb.append("     ,COURSE.TRAIN_COURSE");
		sb.append("     ,COURSE.STUDY_TIME");
		sb.append("     ,COURSE.CATEGORY_ID");
		sb.append("     ,COURSE.VIEW_FLG");
		sb.append("     ,COURSE.UPDATE_NUMBER");
		// カテゴリIDに紐づけてカテゴリ名を取得する
		sb.append("     ,CATEGORY.CATEGORY_NAME");
		sb.append("          from COURSE");
		sb.append("	     inner join CATEGORY on COURSE.CATEGORY_ID = CATEGORY.CATEGORY_ID ");
		// コースIDで降順にする
		sb.append(" where COURSE.COURSE_ID = ?");

		List<CourseDto> list = new ArrayList<>();

		// ステートメントオブジェクトを作成
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, courseId);

			// SQL文を実行
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				CourseDto dto = new CourseDto();
				dto.setCourseId(rs.getInt("COURSE_ID"));
				dto.setCourseName(rs.getString("COURSE_NAME"));
				dto.setCourseOverview(rs.getString("COURSE_OVERVIEW"));
				dto.setPrecondition(rs.getString("PRECONDITION"));
				dto.setGoal(rs.getString("GOAL"));
				dto.setTrainCourse(rs.getString("TRAIN_COURSE"));
				dto.setStudyTime(rs.getInt("STUDY_TIME"));
				dto.setCategoryId(rs.getInt("CATEGORY_ID"));
				dto.setViewFlg(rs.getInt("VIEW_FLG"));
				dto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				dto.setCategoryName(rs.getString("CATEGORY_NAME"));
				list.add(dto);
			}
		}
		return list;
	}
	/**
	 * コースIDに該当するカリキュラムIDの件数を取得する
	 */

	public int selectCountByCourseId(int CurriculumId) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		// コースIDに該当するカリキュラムIDの件数を取得する
		sb.append(" COUNT(CURRICULUM.CURRICULUM_ID)");
		sb.append(" from curricurlum");
		sb.append(" where CURRICULUM.COURSE_ID = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, CurriculumId);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				// 該当件数を返却する
				return rs.getInt(1);
			}
		}
		// 該当するデータがない場合は0を返却する
		return 0;
	}

}