package jp.keronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.OperatorDto;

public class OperatorDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public OperatorDao(Connection conn) {
		this.conn = conn;
	}

	public List<OperatorDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();

		sb.append("   select");
		sb.append("          OPERATOR_ID");
		sb.append("         ,FIRST_NAME");
		sb.append("         ,LAST_NAME");
		sb.append("         ,PASSWORD");
		sb.append("         ,EMAIL");
		sb.append("         ,OPERATOR_DIVISION");
		sb.append("     from OPERATOR");
		sb.append("   order by OPERATOR_ID");

		List<OperatorDto> list = new ArrayList<>();
		// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());
			while (rs.next()) {
				OperatorDto dto = new OperatorDto();
				dto.setOperatorId(rs.getInt("OPERATOR_ID"));
				dto.setFirstName(rs.getString("FIRST_NAME"));
				dto.setLastName(rs.getString("LAST_NAME"));
				dto.setEmail(rs.getString("EMAIL"));
				dto.setOperatorDivision(rs.getString("OPERATOR_DIVISION"));
				list.add(dto);
			}
		}

		return list;

	}

	public OperatorDto selectByOperatorId(OperatorDto dto) throws SQLException {

		// SQL文を作成する
				StringBuffer sb = new StringBuffer();
				sb.append(" select");
				sb.append("        OPERATOR_ID");
				sb.append("       ,FIRST_NAME");
				sb.append("       ,LAST_NAME");
				sb.append("       ,PASSWORD");
				sb.append("       ,EMAIL");
				sb.append("       ,OPERATOR_DIVISION");
				sb.append("   from OPERATOR");
				sb.append("  where OPERATOR_ID = ?");

				// ステートメントオブジェクトを作成する
				try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
					// プレースホルダーに値をセットする
					ps.setInt(1, dto.getOperatorListId());

					// SQL文を実行する
					ResultSet rs = ps.executeQuery();
					if (rs.next()) {
						dto = new OperatorDto();
						dto.setOperatorId(rs.getInt("OPERATOR_ID"));
						dto.setFirstName(rs.getString("FIRST_NAME"));
						dto.setLastName(rs.getString("LAST_NAME"));
						dto.setEmail(rs.getString("EMAIL"));
						dto.setOperatorDivision(rs.getString("OPERATOR_DIVISION"));
					}
				}
				return dto;
	}

	public OperatorDto selectByOperatorListId(OperatorDto dto) throws SQLException {

		// SQL文を作成する
				StringBuffer sb = new StringBuffer();
				sb.append(" select");
				sb.append("        OPERATOR_ID");
				sb.append("       ,FIRST_NAME");
				sb.append("       ,LAST_NAME");
				sb.append("       ,PASSWORD");
				sb.append("       ,EMAIL");
				sb.append("       ,OPERATOR_DIVISION");
				sb.append("   from OPERATOR");
				sb.append("  where OPERATOR_ID = ?");

				// ステートメントオブジェクトを作成する
				try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
					// プレースホルダーに値をセットする
					ps.setInt(1, dto.getOperatorListId());

					// SQL文を実行する
					ResultSet rs = ps.executeQuery();
					if (rs.next()) {
						dto = new OperatorDto();
						dto.setOperatorListId(rs.getInt("OPERATOR_ID"));
						dto.setFirstName(rs.getString("FIRST_NAME"));
						dto.setLastName(rs.getString("LAST_NAME"));
						dto.setEmail(rs.getString("EMAIL"));
						dto.setOperatorDivision(rs.getString("OPERATOR_DIVISION"));
					}
				}
				return dto;
	}


	public int insert(OperatorDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into OPERATOR");
		sb.append("           (");
		sb.append("             FIRST_NAME");
		sb.append("            ,LAST_NAME");
		sb.append("            ,EMAIL");
		sb.append("            ,PASSWORD");
		sb.append("            ,OPERATOR_DIVISION");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("            ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,sha2('CodeTrain123',256)");
		sb.append("            ,?");
		sb.append("           )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getFirstName());
			ps.setString(2, dto.getLastName());
			ps.setString(3, dto.getEmail());
			//ps.setString(4, dto.getPassword());
			ps.setString(4, dto.getOperatorDivision());
			System.out.println(ps.toString());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

	public int update(OperatorDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append(" OPERATOR");
		sb.append(" set");
		sb.append(" FIRST_NAME = ?");
		sb.append(", LAST_NAME = ?");
		sb.append(", EMAIL = ?");
		sb.append(", OPERATOR_DIVISION = ?");
		sb.append(" where OPERATOR_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getFirstName());
			ps.setString(2, dto.getLastName());
			ps.setString(3, dto.getEmail());
			//ps.setString(4, dto.getPassword());
			ps.setString(4, dto.getOperatorDivision());
			ps.setInt(5, dto.getOperatorId());

			System.out.println(ps.toString());


			return ps.executeUpdate();

		}
	}

	public int updatePassword(OperatorDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append(" OPERATOR");
		sb.append(" set");
		sb.append(" PASSWORD = sha2(?, 256)");
		sb.append(" ,UPDATE_NUMBER = UPDATE_NUMBER + 1");
		sb.append(" where OPERATOR_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getPassword());
			ps.setInt(2, dto.getOperatorId());

			return ps.executeUpdate();
		}
	}

	
	public void deleteByOperatorId(int operatorId) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from OPERATOR");
		sb.append("       where OPERATOR_ID = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, operatorId);

			// SQLを実行する
			ps.executeUpdate();
		}
	}


	public int delete(OperatorDto dto) throws SQLException{
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from OPERATOR");
		sb.append("       where OPERATOR_ID = ?");
		sb.append("         and UPDATE_NUMBER = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getOperatorId());
			ps.setInt(2, dto.getUpdateNumber());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

	public OperatorDto findByIdAndPassword(String id, String password) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" OPERATOR_ID");
		sb.append(" ,EMAIL");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,OPERATOR_DIVISION");
		sb.append(" ,UPDATE_NUMBER");
		sb.append(" from OPERATOR");
		sb.append(" where EMAIL = ?");
		sb.append(" and PASSWORD = sha2(?, 256)");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setString(1, id);
			ps.setString(2, password);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				OperatorDto operator = new OperatorDto();
				operator.setOperatorId(rs.getInt("OPERATOR_ID"));
				operator.setEmail(rs.getString("EMAIL"));
				operator.setFirstName(rs.getString("FIRST_NAME"));
				operator.setLastName(rs.getString("LAST_NAME"));
				operator.setOperatorDivision(rs.getString("OPERATOR_DIVISION"));
				operator.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				return operator;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	public OperatorDto findPasswordByOperatorId(int id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" OPERATOR_ID");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,UPDATE_NUMBER");
		sb.append(" ,PASSWORD");
		sb.append(" from OPERATOR");
		sb.append(" where OPERATOR_ID = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				OperatorDto operator = new OperatorDto();
				operator.setOperatorId(rs.getInt("OPERATOR_ID"));
				operator.setFirstName(rs.getString("FIRST_NAME"));
				operator.setLastName(rs.getString("LAST_NAME"));
				//operator.setOperatorId(rs.getInt("COMPANY_ID"));
				operator.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				operator.setPassword(rs.getString("PASSWORD"));
				return operator;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}


}



