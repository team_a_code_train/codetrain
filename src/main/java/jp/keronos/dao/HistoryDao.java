package jp.keronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.keronos.dto.HistoryDto;
/**
 * ナレッジテーブルのDataAccessObject
 */
public class HistoryDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public HistoryDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * ユーザIDに該当するカリキュラム履歴を取得する(履修済み)
	 * ナレッジIDで降順にする
	 * @throws SQLException SQL例外
	 */
	public int selectCountByUserIdAndCategoryId(int userId, int categoryId, int courseId) throws SQLException {

		//SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		//ユーザIDに該当するカリキュラムIDの件数を取得する(履修済み)
		sb.append(" COUNT(COURSE_ID)");
		sb.append(" from HISTORY");
		sb.append(" where USER_ID = ?");
		sb.append(" and CATEGORY_ID = ?");
		sb.append(" and COURSE_ID = ?");

		//ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, userId);
			ps.setInt(2, categoryId);
			ps.setInt(3, courseId);
			
			//SQL文を実行する
			ResultSet rs = ps.executeQuery();
			if (rs.next()){
				HistoryDto dto = new HistoryDto();
				dto.setCourseIdCount(rs.getInt("COUNT(COURSE_ID)"));
				// 学習したカリキュラムの数(ユーザIDとカテゴリIDに紐づくコースIDの数)を返す
				return dto.getCourseIdCount();
			}
			// 該当するデータがない場合は0を返却する
			return 0;
		}
	}
	
	public int insert(HistoryDto dto) throws SQLException {

    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into HISTORY");
		sb.append("           (");
		sb.append("             USER_ID");
		sb.append("            ,CATEGORY_ID");
		sb.append("            ,COURSE_ID");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getUserId());
			ps.setInt(2, dto.getCategoryId());
			ps.setInt(3, dto.getCourseId());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

}
