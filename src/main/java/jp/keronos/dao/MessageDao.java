package jp.keronos.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.MessageDto;
import jp.keronos.dto.OperatorDto;

/**
 * メッセージテーブルのDataAccessObject
 * 
 * @author Mr.X
 */
public class MessageDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ コネクションをフィールドに設定する
	 * 
	 * @param conn コネクション
	 */
	public MessageDao(Connection conn) {
		this.conn = conn;
	}

	public  List<MessageDto> selectByUserId(int userId) throws SQLException {

		// SQL文を作成
		StringBuffer sb = new StringBuffer();
		sb.append("     select");
		sb.append("            MESSAGE_ID");
		sb.append("           ,MESSAGE");
		//sb.append("           ,USER_ID");
		//sb.append("           ,OPERATOR_ID");
		sb.append("           ,DATE");
		//sb.append("           ,COMPANY_ID");
		sb.append("       from MESSAGE ");
		sb.append("      where USER_ID = ?");
		sb.append("   order by DATE desc");

		List<MessageDto> list = new ArrayList<>();

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, userId);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				MessageDto dto = new MessageDto();
				dto.setMessageId(rs.getInt("MESSAGE_ID"));
				dto.setMessage(rs.getString("MESSAGE"));
				//dto.setUserId(userId);
				//dto.setOpearatorId(rs.getInt("OPERATOR_ID"));
				dto.setDate(rs.getTimestamp("DATE"));
				list.add(dto);
			}
		}
		return list;
	}

	public List<MessageDto> selectByUserIdFromOperator(int userId) throws SQLException {

		// SQL文を作成する
				StringBuffer sb = new StringBuffer();
				sb.append(" select");
				sb.append("            MESSAGE_ID");
				sb.append("           ,MESSAGE");
				sb.append("           ,DATE");
				sb.append("   from MESSAGE");
				sb.append("  where USER_ID = ?");
				sb.append("   order by DATE desc");
				
				List<MessageDto> list = new ArrayList<>();
				
				// ステートメントオブジェクトを作成する
				try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
					// プレースホルダーに値をセットする
					ps.setInt(1, userId);

					// SQL文を実行する
					ResultSet rs = ps.executeQuery();
					while(rs.next()) {
						MessageDto dto = new MessageDto();
						dto.setMessageId(rs.getInt("MESSAGE_ID"));
						dto.setMessage(rs.getString("MESSAGE"));
						dto.setDate(rs.getTimestamp("DATE"));
						dto.setUserId(userId);
						list.add(dto);
					}
				}
				return list;
	}
	
	public int insert(MessageDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into MESSAGE");
		sb.append("           (");
		sb.append("             MESSAGE");
		sb.append("            ,USER_ID");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("           )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getMessage());
			ps.setInt(2, dto.getUserId());
			
			// SQLを実行する
			return ps.executeUpdate();
		}
	}
	
	public int operatorInsert(MessageDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into MESSAGE");
		sb.append("           (");
		sb.append("             MESSAGE");
		sb.append("            ,USER_ID");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("           )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getMessage());
			ps.setInt(2, dto.getUserId());
			System.out.println(ps.toString());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}
	
	
	
	public List<MessageDto> selectUserList() throws SQLException {
		
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("     select");
		sb.append("            m.MESSAGE_ID");
		sb.append("           ,m.USER_ID");
		sb.append("           ,u.FIRST_NAME");
		sb.append("           ,u.LAST_NAME");
		sb.append("           ,u.COMPANY_NAME");
		sb.append("           ,m.DATE");
		sb.append("       from MESSAGE m");
		sb.append(" inner join USER u");
		sb.append("         on m.USER_ID = u.USER_ID");
		sb.append("   order by m.DATE desc");

		List<MessageDto> list = new ArrayList<>();

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			//ps.setInt(1, userListId);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MessageDto dto = new MessageDto();

				dto.setMessageId(rs.getInt("MESSAGE_ID"));
				dto.setUserId(rs.getInt("USER_ID"));
				dto.setFirstName(rs.getString("FIRST_NAME"));
				dto.setLastName(rs.getString("LAST_NAME"));
				dto.setCompanyName(rs.getString("COMPANY_NAME"));
				dto.setDate(rs.getTimestamp("DATE"));
				list.add(dto);
			}
		}
		return list;
	}
	
}