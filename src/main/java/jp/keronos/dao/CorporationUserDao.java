package jp.keronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.CorporationUserDto;

public class CorporationUserDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public CorporationUserDao(Connection conn) {
		this.conn = conn;
	}

	public CorporationUserDto findByIdAndPassword(String id, String password) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" COMPANY_USER_ID");
		sb.append(" ,EMAIL");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,COMPANY_ID");
		sb.append(" ,COMPANY_NAME");
		sb.append(" ,UPDATE_NUMBER");
		sb.append(" from COMPANY_USER");
		sb.append(" where EMAIL = ?");
		sb.append(" and PASSWORD = sha2(?, 256)");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setString(1, id);
			ps.setString(2, password);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				CorporationUserDto corporationUser = new CorporationUserDto();
				corporationUser.setUserId(rs.getInt("COMPANY_USER_ID"));
				corporationUser.setEmail(rs.getString("EMAIL"));
				corporationUser.setFirstName(rs.getString("FIRST_NAME"));
				corporationUser.setLastName(rs.getString("LAST_NAME"));
				corporationUser.setCompanyId(rs.getInt("COMPANY_ID"));
				corporationUser.setCompanyName(rs.getString("COMPANY_NAME"));
				corporationUser.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				return corporationUser;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	public CorporationUserDto findPasswordByUserId(int id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" COMPANY_USER_ID");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,COMPANY_ID");
		sb.append(" ,UPDATE_NUMBER");
		sb.append(" ,PASSWORD");
		sb.append(" from COMPANY_USER");
		sb.append(" where COMPANY_USER_ID = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				CorporationUserDto corporationUser = new CorporationUserDto();
				corporationUser.setUserId(rs.getInt("COMPANY_USER_ID"));
				corporationUser.setFirstName(rs.getString("FIRST_NAME"));
				corporationUser.setLastName(rs.getString("LAST_NAME"));
				corporationUser.setCompanyId(rs.getInt("COMPANY_ID"));
				corporationUser.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				corporationUser.setPassword(rs.getString("PASSWORD"));
				return corporationUser;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	public int update(CorporationUserDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append(" COMPANY_USER");
		sb.append(" set");
		sb.append(" PASSWORD = sha2(?, 256)");
		sb.append(" ,UPDATE_NUMBER = UPDATE_NUMBER + 1");
		sb.append(" where COMPANY_USER_ID = ?");
		sb.append(" and UPDATE_NUMBER = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getPassword());
			ps.setInt(2, dto.getUserId());
			ps.setInt(3, dto.getUpdateNumber());

			return ps.executeUpdate();
		}
	}

/**
 * 企業アカウント情報を追加する
 * @param dto 企業アカウント情報
 * @return 更新件数
 * @throws SQLException SQL例外
 */
	public int insert(CorporationUserDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into COMPANY_USER");
		sb.append("           (");
		sb.append(" EMAIL");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,COMPANY_NAME");
		sb.append(" ,COMPANY_ID");
		sb.append(" ,PASSWORD");
		sb.append(" ,UPDATE_NUMBER");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,sha2('CodeTrain123',256)");
		sb.append("            ,0");
		sb.append("           )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getEmail());
			ps.setString(2, dto.getFirstName());
			ps.setString(3, dto.getLastName());
			ps.setString(4, dto.getCompanyName());
			ps.setInt(5, dto.getCompanyId());
			//ps.setString(6, dto.getPassword());
			// SQLを実行する
			return ps.executeUpdate();
		}
	}
	public List<CorporationUserDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append(" COMPANY_USER_ID");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,COMPANY.COMPANY_NAME");
		sb.append(" ,COMPANY_USER.COMPANY_ID");
		sb.append(" ,COMPANY_USER.EMAIL");
		sb.append(" ,COMPANY_USER.PASSWORD");
		sb.append("   from COMPANY_USER");
		sb.append(" inner join COMPANY on COMPANY.COMPANY_ID = COMPANY_USER.COMPANY_ID");
		sb.append("  order by COMPANY_USER_ID");


		List<CorporationUserDto> list = new ArrayList<>();
    	// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());
			while (rs.next()) {
				CorporationUserDto dto = new CorporationUserDto();
				dto.setUserId(rs.getInt("COMPANY_USER_ID"));
				dto.setFirstName(rs.getString("FIRST_NAME"));
				dto.setLastName(rs.getString("LAST_NAME"));
				dto.setCompanyId(rs.getInt("COMPANY_ID"));
				dto.setCompanyName(rs.getString("COMPANY_NAME"));
				dto.setEmail(rs.getString("EMAIL"));
				dto.setPassword(rs.getString("PASSWORD"));
				list.add(dto);
			}
			return list;
		}
	}

	public CorporationUserDto findByCorporationUserId(int id) throws SQLException {


		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" COMPANY_USER_ID");
		sb.append(" ,COMPANY_ID");
		sb.append(" ,EMAIL");
		sb.append(" ,COMPANY_NAME");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,UPDATE_NUMBER");
		sb.append(" from COMPANY_USER");
		sb.append(" where COMPANY_USER_ID = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				CorporationUserDto corporationUser = new CorporationUserDto();
				corporationUser.setUserId(rs.getInt("COMPANY_USER_ID"));
				corporationUser.setCompanyId(rs.getInt("COMPANY_ID"));
				corporationUser.setEmail(rs.getString("EMAIL"));
				corporationUser.setCompanyName(rs.getString("COMPANY_NAME"));
				corporationUser.setFirstName(rs.getString("FIRST_NAME"));
				corporationUser.setLastName(rs.getString("LAST_NAME"));
				corporationUser.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				return corporationUser;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}
			public int userUpdate(CorporationUserDto dto) throws SQLException {

				// SQL文を作成する
				StringBuffer sb = new StringBuffer();
				sb.append(" update");
				sb.append(" COMPANY_USER");
				sb.append(" set");
				sb.append(" COMPANY_ID = ?");
				sb.append(" ,COMPANY_NAME = ?");
				sb.append(" ,FIRST_NAME = ?");
				sb.append(" ,LAST_NAME = ?");
				sb.append(" ,EMAIL = ?");
				//sb.append(" UPDATE_NUMBER = UPDATE_NUMBER + 1");
				sb.append(" where COMPANY_USER_ID = ?");
				//sb.append(" and UPDATE_NUMBER = ?");

				// ステートメントオブジェクトを作成する
				try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

					// プレースホルダーに値をセットする
					ps.setInt(1, dto.getCompanyId());
					ps.setString(2, dto.getCompanyName());
					ps.setString(3, dto.getFirstName());
					ps.setString(4, dto.getLastName());
					ps.setString(5, dto.getEmail());
					ps.setInt(6, dto.getUserId());
					//ps.setInt(7, dto.getUpdateNumber());

					// SQLを実行する
					return ps.executeUpdate();
				}
			}


public CorporationUserDto selectByCorporationUserListId(CorporationUserDto dto) throws SQLException {

	// SQL文を作成する
			StringBuffer sb = new StringBuffer();
			sb.append(" select");
			sb.append(" COMPANY_USER_ID");
			sb.append(" ,FIRST_NAME");
			sb.append(" ,LAST_NAME");
			sb.append(" ,COMPANY_ID");
			sb.append(" ,COMPANY_NAME");
			sb.append(" ,EMAIL");
			sb.append(" ,PASSWORD");
			sb.append("   from COMPANY_USER");
			sb.append("  where COMPANY_USER_ID = ?");

			// ステートメントオブジェクトを作成する
			try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
				// プレースホルダーに値をセットする
				ps.setInt(1, dto.getCorporationUserListId());

				System.out.println(ps.toString());
				// SQL文を実行する
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					CorporationUserDto corporationUser = new CorporationUserDto();
					corporationUser.setCorporationUserListId(rs.getInt("CORPORATION_USER_ID"));
					corporationUser.setFirstName(rs.getString("FIRST_NAME"));
					corporationUser.setLastName(rs.getString("LAST_NAME"));
					corporationUser.setCompanyId(rs.getInt("COMPANY_ID"));
					corporationUser.setCompanyName(rs.getString("COMPANY_NAME"));
					corporationUser.setEmail(rs.getString("EMAIL"));
					corporationUser.setPassword(rs.getString("PASSWORD"));
					corporationUser.setEmail(rs.getString("EMAIL"));
					return corporationUser;
				}
				return null;
			}

			}
public List<CorporationUserDto> selectByCompanyId(int companyId) throws SQLException {

	// SQL文を作成する
			StringBuffer sb = new StringBuffer();
			sb.append(" select");
			sb.append(" COMPANY_USER_ID");
			sb.append(" ,FIRST_NAME");
			sb.append(" ,LAST_NAME");
			sb.append(" ,COMPANY.COMPANY_NAME");
			sb.append(" ,COMPANY_USER.COMPANY_ID");
			sb.append(" ,COMPANY_USER.EMAIL");
			sb.append("   from COMPANY_USER");
			sb.append(" inner join COMPANY on COMPANY.COMPANY_ID = COMPANY_USER.COMPANY_ID");
			sb.append("  where COMPANY_USER.COMPANY_ID = ?");
			List<CorporationUserDto> list = new ArrayList<>();
			// ステートメントオブジェクトを作成する
			try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
				// プレースホルダーに値をセットする
				ps.setInt(1, companyId);

				System.out.println(ps.toString());
				// SQL文を実行する
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					CorporationUserDto dto = new CorporationUserDto();
					dto.setUserId(rs.getInt("COMPANY_USER_ID"));
					dto.setFirstName(rs.getString("FIRST_NAME"));
					dto.setLastName(rs.getString("LAST_NAME"));
					dto.setCompanyName(rs.getString("COMPANY_NAME"));
					dto.setCompanyId(rs.getInt("COMPANY_ID"));
					dto.setEmail(rs.getString("EMAIL"));
					list.add(dto);
				}
			}
			return list;
			}

public CorporationUserDto selectByCorporationUserId(CorporationUserDto dto ) throws SQLException {

	// SQL文を作成する
			StringBuffer sb = new StringBuffer();
			sb.append(" select");
			sb.append(" COMPANY_USER_ID");
			sb.append(" ,FIRST_NAME");
			sb.append(" ,LAST_NAME");
			sb.append(" ,COMPANY_ID");
			sb.append(" ,COMPANY_NAME");
			sb.append(" ,EMAIL");
			sb.append("   from COMPANY_USER");
			sb.append("  where COMPANY_USER_ID = ?");

			// ステートメントオブジェクトを作成する
			try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
				// プレースホルダーに値をセットする
				ps.setInt(1, dto.getUserId());

				// SQL文を実行する
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					dto = new CorporationUserDto();
					dto.setUserId(rs.getInt("COMPANY_USER_ID"));
					dto.setFirstName(rs.getString("FIRST_NAME"));
					dto.setLastName(rs.getString("LAST_NAME"));
					dto.setCompanyId(rs.getInt("COMPANY_ID"));
					dto.setCompanyName(rs.getString("COMPANY_NAME"));
					dto.setEmail(rs.getString("EMAIL"));

				}
			}
			return dto;
			}


public int delete(CorporationUserDto dto) throws SQLException {

	// SQL文を作成する
	StringBuffer sb = new StringBuffer();
	sb.append(" delete from COMPANY_USER");
	sb.append("       where COMPANY_USER_ID = ?");
	sb.append("         and UPDATE_NUMBER = ?");

	// ステートメントオブジェクトを作成する
	try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
    	// プレースホルダーに値をセットする
		ps.setInt(1, dto.getUserId());
		ps.setInt(2, dto.getUpdateNumber());

        // SQLを実行する
		return ps.executeUpdate();
	}
}
public void deleteByCorporationUserId(int userId) throws SQLException {

	// SQL文を作成する
	StringBuffer sb = new StringBuffer();
	sb.append(" delete from COMPANY_USER");
	sb.append("       where COMPANY_USER_ID = ?");

	// ステートメントオブジェクトを作成する
	try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
		// プレースホルダーに値をセットする
		ps.setInt(1, userId);
		System.out.println(ps.toString());

		// SQLを実行する
		ps.executeUpdate();
	}
}
}







