package jp.keronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.CourseDto;
import jp.keronos.dto.ListCurriculumDto;

/**
 * ナレッジテーブルのDataAccessObject
 * 
 * @author Mr.X
 */
public class ListCurriculumDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ コネクションをフィールドに設定する
	 * 
	 * @param conn コネクション
	 */
	public ListCurriculumDao(Connection conn) {
		this.conn = conn;
	}

	/**
	 * コース一覧情報リストを全件取得する コースIDで昇順にする
	 * 
	 * @param コースID
	 * @throws SQLException SQL例外
	 */
	public List<CourseDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		// コース一覧情報リストを全件取得する
		sb.append("      COURSE.COURSE_ID");
		sb.append("     ,COURSE.COURSE_NAME");
		sb.append("     ,COURSE.COURSE_OVERVIEW");
		sb.append("     ,COURSE.PRECONDITION");
		sb.append("     ,COURSE.GOAL");
		sb.append("     ,COURSE.TRAIN_COURSE");
		sb.append("     ,COURSE.STUDY_TIME");
		sb.append("     ,COURSE.CURRICULUM_ID");
		sb.append("     ,COURSE.CATEGORY_ID");
		sb.append("     ,COURSE.VIEW_FLG");
		sb.append("     ,COURSE.UPDATE_NUMBER");
		// カテゴリIDに紐づくカテゴリ名の取得
		sb.append("     ,CATEGORY.CATEGORY_NAME");
		sb.append("     from COURSE");
		sb.append("     inner join CATEGORY on COURSE.CATEGORY_ID = CATEGORY.CATEGORY_ID ");
		// ナレッジIDで降順にする
		sb.append(" order by COURSE_ID desc");

		List<CourseDto> list = new ArrayList<>();

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				CourseDto dto = new CourseDto();
				dto.setCourseId(rs.getInt("COURSE_ID"));
				dto.setCourseName(rs.getString("COURSE_NAME"));
				dto.setCourseOverview(rs.getString("COURSE_OVERVIEW"));
				dto.setPrecondition(rs.getString("PRECONDITION"));
				dto.setGoal(rs.getString("GOAL"));
				dto.setTrainCourse(rs.getString("TRAIN_COURSE"));
				dto.setStudyTime(rs.getInt("STUDY_TIME"));
				dto.setCurriculumId(rs.getInt("CURRICULUM_ID"));
				dto.setCategoryId(rs.getInt("CATEGORY_ID"));
				dto.setViewFlg(rs.getInt("VIEW_FLG"));
				dto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				dto.setCategoryName(rs.getString("CATEGORY_NAME"));
				list.add(dto);
			}
		}
		return list;
	}

	public List<ListCurriculumDto> selectByCourseId(int courseId) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		// コース一覧情報リストを全件取得する
		sb.append("     CURRICULUM_ID");
		sb.append("     ,CURRICULUM");
		sb.append("     ,TEXT");
		sb.append("     ,COURSE_ID");
		sb.append("     ,CATEGORY_ID");
		sb.append("     ,UPDATE_NUMBER");
		sb.append("     from CURRICULUM");
		sb.append(" where COURSE_ID = ?");
		sb.append(" order by CURRICULUM_ID");

		List<ListCurriculumDto> list = new ArrayList<>();

		// ステートメントオブジェクトを作成
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, courseId);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ListCurriculumDto dto = new ListCurriculumDto();
				dto.setCurriculumId(rs.getInt("CURRICULUM_ID"));
				dto.setCurriculum(rs.getString("CURRICULUM"));
				dto.setText(rs.getString("TEXT"));
				dto.setCourseId(rs.getInt("COURSE_ID"));
				dto.setCategoryId(rs.getInt("CATEGORY_ID"));
				dto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				list.add(dto);
			}
		}
		return list;
	}

	/**
	 * コースIDに該当するカリキュラムIDの件数を取得する（全カリキュラム数）
	 * 
	 * @throws SQLException SQL例外
	 */
	public List<ListCurriculumDto> selectByCurriculumId(int curriculumId) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		// コース一覧情報リストを全件取得する
		sb.append("     CURRICULUM_ID");
		sb.append("     ,CURRICULUM");
		sb.append("     ,TEXT");
		sb.append("     ,COURSE_ID");
		sb.append("     ,CATEGORY_ID");
		sb.append("     ,UPDATE_NUMBER");
		sb.append("     from CURRICULUM");
		sb.append(" where CURRICULUM_ID = ?");
		sb.append(" order by CURRICULUM_ID");

		List<ListCurriculumDto> list = new ArrayList<>();

		// ステートメントオブジェクトを作成
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, curriculumId);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ListCurriculumDto dto = new ListCurriculumDto();
				dto.setCurriculumId(rs.getInt("CURRICULUM_ID"));
				dto.setCurriculum(rs.getString("CURRICULUM"));
				dto.setText(rs.getString("TEXT"));
				dto.setCourseId(rs.getInt("COURSE_ID"));
				dto.setCategoryId(rs.getInt("CATEGORY_ID"));
				dto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				list.add(dto);
			}
		}
		return list;
	}
	
	
	public int selectCountByCourseId(int courseId) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		// ユーザIDに該当するカリキュラムIDの件数を取得する(全カリキュラム数)
		sb.append(" COUNT(CURRICULUM_ID)");
		sb.append(" from CURRICULUM");
		sb.append(" where COURSE_ID = ?");
		sb.append(" order by CURRICULUM_ID desc");

		List<CourseDto> list = new ArrayList<>();
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, courseId);

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				CourseDto dto = new CourseDto();
				dto.setCurriculumCount(rs.getInt("COUNT(CURRICULUM_ID)"));
				list.add(dto);

				// カリキュラムの総数を返す
				return dto.getCurriculumCount();
			}
		}
		// カリキュラムの数が存在しない場合は0を返す
		return 0;
	}

	/**
	 * コースIDに該当する、カリキュラム情報リストを取得する（コース詳細のカリキュラム表示するためのやつ） カリキュラムIDで降順にする
	 * 
	 * @throws SQLException SQL例外
	 */

}