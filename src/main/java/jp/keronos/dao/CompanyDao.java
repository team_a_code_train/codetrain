package jp.keronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.CompanyDto;

public class CompanyDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public CompanyDao(Connection conn) {
        this.conn = conn;
    }

	/**
	 * チャンネル情報を取得する
	 * @return チャンネル情報リスト
	 * @throws SQLException SQL例外
	 */
	public List<CompanyDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          COMPANY_ID");
		sb.append("         ,COMPANY_NAME");
		sb.append("         ,EMAIL");
		sb.append("     from COMPANY");
		sb.append(" order by COMPANY_ID");


		List<CompanyDto> list = new ArrayList<>();
    	// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());
			while (rs.next()) {
				CompanyDto dto = new CompanyDto();
				dto.setCompanyId(rs.getInt("COMPANY_ID"));
				dto.setCompanyName(rs.getString("COMPANY_NAME"));
				dto.setEmail(rs.getString("EMAIL"));
				list.add(dto);
			}
			return list;
		}
	}

	public int update(CompanyDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append(" COMPANY");
		sb.append(" set");
		sb.append(" COMPANY_NAME = ?");
		sb.append(", EMAIL = ?");
		sb.append(" where COMPANY_ID = ?");

		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getCompanyName());
			ps.setString(2, dto.getEmail());
			ps.setInt(3, dto.getCompanyId());

			System.out.println(ps.toString());

			return ps.executeUpdate();

		}
	}

	public CompanyDto selectByCompanyId(CompanyDto dto) throws SQLException {

		// SQL文を作成する
				StringBuffer sb = new StringBuffer();
				sb.append(" select");
				sb.append("        COMPANY_ID");
				sb.append("       ,COMPANY_NAME");
				sb.append("       ,EMAIL");
				sb.append("   from COMPANY");
				sb.append("  where COMPANY_ID = ?");

				// ステートメントオブジェクトを作成する
				try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
					// プレースホルダーに値をセットする
					ps.setInt(1, dto.getCompanyListId());

					// SQL文を実行する
					ResultSet rs = ps.executeQuery();
					if (rs.next()) {
						dto = new CompanyDto();
						dto.setCompanyId(rs.getInt("COMPANY_ID"));
						dto.setCompanyName(rs.getString("COMPANY_NAME"));
						dto.setEmail(rs.getString("EMAIL"));

					}
				}
				return dto;
	}

	public int insert(CompanyDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();

		sb.append(" insert into");
		sb.append(" COMPANY");
		sb.append(" (");
		sb.append(" COMPANY_NAME");
		sb.append(" ,EMAIL");
		sb.append(" )");
		sb.append(" VALUES");
		sb.append(" (");
		sb.append(" ?");
		sb.append(", ?");
		sb.append(" )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getCompanyName());
			ps.setString(2, dto.getEmail());
			System.out.println(ps.toString());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

	public void deleteByCompanyId(int companyId) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from COMPANY");
		sb.append("       where COMPANY_ID = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, companyId);
			System.out.println(ps.toString());

			// SQLを実行する
			ps.executeUpdate();
		}
	}
}
