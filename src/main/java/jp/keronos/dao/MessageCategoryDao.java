package jp.keronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.MessageCategoryDto;

public class MessageCategoryDao {
	
	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public MessageCategoryDao(Connection conn) {
		this.conn = conn;
	}
	
	public List<MessageCategoryDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();

		sb.append("   select");
		sb.append("          MESSAGE_CATEGORY_ID");
		sb.append("         ,MESSAGE_CATEGORY_NAME");
		sb.append("     from MESSAGE_CATEGORY");

		List<MessageCategoryDto> list = new ArrayList<>();
		// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());
			while (rs.next()) {
				MessageCategoryDto dto = new MessageCategoryDto();
				dto.setMessageCategoryId(rs.getInt("MESSAGE_CATEGORY_ID"));
				dto.setMessageCategoryName(rs.getString("MESSAGE_CATEGORY_NAME"));
				list.add(dto);
			}
		}
		return list;
	}
}
