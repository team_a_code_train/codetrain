package jp.keronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.UserDto;

public class UserDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定する
	 * @param conn コネクション
	 */
	public UserDao(Connection conn) {
		this.conn = conn;
	}

	public List<UserDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" USER_ID");
		sb.append(" ,EMAIL");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,COMPANY_ID");
		sb.append(" ,COMPANY_NAME");
		sb.append(" ,SKILL");
		sb.append(" ,REASON");
		sb.append(" ,STATUS_FLG");
		sb.append(" ,FREEZE_DATE");
		sb.append(" ,UPDATE_NUMBER");
		sb.append(" ,DEL_FLG");
		sb.append(" ,DEL_DATE");
		sb.append(" from USER");
		sb.append(" order by USER_ID");

		List<UserDto> list = new ArrayList<>();
		// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());
			while (rs.next()) {
				UserDto dto = new UserDto();
				dto.setUserId(rs.getInt("USER_ID"));
				dto.setFirstName(rs.getString("FIRST_NAME"));
				dto.setLastName(rs.getString("LAST_NAME"));
				dto.setCompanyId(rs.getInt("COMPANY_ID"));
				dto.setCompanyName(rs.getString("COMPANY_NAME"));
				dto.setSkill(rs.getString("SKILL"));
				dto.setReason(rs.getString("REASON"));
				dto.setStatus_flg(rs.getBoolean("STATUS_FLG"));
				dto.setFreeze_date(rs.getTimestamp("FREEZE_DATE"));
				dto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				dto.setDel_flg(rs.getBoolean("DEL_FLG"));
				dto.setDel_date(rs.getTimestamp("DEL_DATE"));
				list.add(dto);
			}
			return list;
		}
	}

	public UserDto findByIdAndPassword(String id, String password) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" USER_ID");
		sb.append(" ,EMAIL");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,UPDATE_NUMBER");
		sb.append(" ,STATUS_FLG");
		sb.append(" ,DEL_FLG");
		sb.append(" from USER");
		sb.append(" where EMAIL = ?");
		sb.append(" and PASSWORD = sha2(?, 256)");
		sb.append(" and DEL_FLG = 0");
		sb.append(" and STATUS_FLG = 0");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setString(1, id);
			ps.setString(2, password);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				UserDto user = new UserDto();
				user.setUserId(rs.getInt("USER_ID"));
				user.setEmail(rs.getString("EMAIL"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				user.setDel_flg(rs.getBoolean("DEL_FLG"));
				user.setStatus_flg(rs.getBoolean("STATUS_FLG"));
				return user;
			}
			// 該当するデータがない場合はnullを戻す
			return null;
		}
	}

	public UserDto findByUserIdAndPassword(int userId, String pass1) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" USER_ID");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,PASSWORD");
		sb.append(" from USER");
		sb.append(" where USER_ID = ?");
		sb.append(" and PASSWORD = sha2(?, 256)");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, userId);
			ps.setString(2, pass1);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				UserDto user = new UserDto();
				user.setUserId(rs.getInt("USER_ID"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setPassword(rs.getString("PASSWORD"));
				return user;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	public UserDto findByUserId(int id) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append(" USER_ID");
		sb.append(" ,EMAIL");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,SKILL");
		sb.append(" ,UPDATE_NUMBER");
		sb.append(" ,STATUS_FLG");
		sb.append(" ,DEL_FLG");
		sb.append(" from USER");
		sb.append(" where USER_ID = ?");
		sb.append(" and DEL_FLG = 0");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, id);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				UserDto user = new UserDto();
				user.setUserId(rs.getInt("USER_ID"));
				user.setEmail(rs.getString("EMAIL"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setSkill(rs.getString("SKILL"));
				user.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				user.setStatus_flg(rs.getBoolean("STATUS_FLG"));
				user.setDel_flg(rs.getBoolean("DEL_FLG"));
				return user;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	public UserDto countUserByCompanyId(int companyId) throws SQLException {

		//SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		//会社IDに該当する、法人利用者の人数と凍結フラグの数を取得する
		sb.append(" COUNT(USER_ID)");
		sb.append(" ,SUM(CASE WHEN STATUS_FLG = 1 then 1 else 0 end) as FLG_CNT");
		sb.append(" from USER");
		sb.append(" where COMPANY_ID = ?");

		//ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, companyId);

			//SQL文を実行する
			ResultSet rs = ps.executeQuery();
			if (rs.next()){
				UserDto dto = new UserDto();
				dto.setUserIdCount(rs.getInt("COUNT(USER_ID)"));
				dto.setStatus_flgCount(rs.getInt("FLG_CNT"));
				// 学習したカリキュラムの数(ユーザIDとカテゴリIDに紐づくコースIDの数)を返す
				return dto;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	 public int updatePassword(UserDto dto) throws SQLException {

     	// SQL文を作成する
 		StringBuffer sb = new StringBuffer();
 		sb.append(" update USER");
 		sb.append(" set");
 		sb.append(" PASSWORD = sha2(?, 256)");
 		sb.append(" ,UPDATE_NUMBER = UPDATE_NUMBER + 1");
 		sb.append(" where USER_ID = ?");

     	// ステートメントオブジェクトを作成する
 		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

         	// プレースホルダーに値をセットする
 			ps.setString(1, dto.getPassword());
 			ps.setInt(2, dto.getUserId());

             // SQLを実行する
 			return ps.executeUpdate();
 		}
 	}

	public int updateSkill(UserDto dto) throws SQLException {

     	// SQL文を作成する
 		StringBuffer sb = new StringBuffer();
 		sb.append(" update USER");
 		sb.append(" set");
 		sb.append(" skill = ?");
 		sb.append(" ,UPDATE_NUMBER = UPDATE_NUMBER + 1");
 		sb.append(" where USER_ID = ?");

     	// ステートメントオブジェクトを作成する
 		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

         	// プレースホルダーに値をセットする
 			ps.setString(1, dto.getSkill());
 			ps.setInt(2, dto.getUserId());

             // SQLを実行
 			return ps.executeUpdate();
 		}
 	}

	// 法人利用者を凍結させるメソッド
	public int freeze(UserDto dto) throws SQLException {

     	// SQL文を作成する
 		StringBuffer sb = new StringBuffer();
 		sb.append(" update USER");
 		sb.append(" set");
 		sb.append(" `STATUS_FLG` = 1 ");
 		sb.append(" ,UPDATE_NUMBER = UPDATE_NUMBER + 1");
 		sb.append(" ,REASON = ?");
 		sb.append(" ,FREEZE_DATE = current_timestamp");
 		sb.append(" where USER_ID = ?");
 		sb.append(" and UPDATE_NUMBER = ?");

     	// ステートメントオブジェクトを作成する
 		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

         	// プレースホルダーに値をセットする
 			ps.setString(1, dto.getReason());
 			ps.setInt(2, dto.getUserId());
 			ps.setInt(3, dto.getUpdateNumber());

             // SQLを実行する
 			return ps.executeUpdate();
 		}
 	}

	// 法人利用者を復帰させるメソッド
	public int revival(UserDto dto) throws SQLException {

     	// SQL文を作成する
 		StringBuffer sb = new StringBuffer();
 		sb.append(" update USER");
 		sb.append(" set");
 		sb.append(" `STATUS_FLG` = 0 ");
 		sb.append(" ,UPDATE_NUMBER = UPDATE_NUMBER + 1");
 		sb.append(" ,REASON = null");
 		sb.append(" ,FREEZE_DATE = null");
 		sb.append(" where USER_ID = ?");
 		sb.append(" and UPDATE_NUMBER = ?");

     	// ステートメントオブジェクトを作成する
 		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

         	// プレースホルダーに値をセットする
 			ps.setInt(1, dto.getUserId());
 			ps.setInt(2, dto.getUpdateNumber());

             // SQLを実行する
 			return ps.executeUpdate();
 		}
 	}

	public int insert(UserDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into USER");
		sb.append(" (");
		sb.append(" EMAIL");
		sb.append(" ,PASSWORD");
		sb.append(" ,FIRST_NAME");
		sb.append(" ,LAST_NAME");
		sb.append(" ,COMPANY_NAME");
		sb.append(" ,COMPANY_ID");
		sb.append(" ,STATUS_FLG");
		sb.append(" ,UPDATE_NUMBER");
		sb.append(" ,DEL_FLG");
		sb.append(" )");
		sb.append(" values");
		sb.append(" (?, sha2(?, 256), ?, ?, ?, ?, 0, 0, 0)");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getEmail());
			ps.setString(2, "CodeTrain123");
			ps.setString(3, dto.getFirstName());
			ps.setString(4, dto.getLastName());
			ps.setString(5, dto.getCompanyName());
			ps.setInt(6, dto.getCompanyId());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

	public int deleteByUserId(int userId, int updateNumber) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update USER");
 		sb.append(" set");
 		sb.append(" `DEL_FLG` = 1 ");
 		sb.append(" ,UPDATE_NUMBER = UPDATE_NUMBER + 1");
 		sb.append(" ,DEL_DATE = current_timestamp");
 		sb.append(" where USER_ID = ?");
 		sb.append(" and UPDATE_NUMBER = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, userId);
			ps.setInt(2, updateNumber);

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

}
