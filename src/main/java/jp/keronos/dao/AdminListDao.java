package jp.keronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.AdminListDto;


public class AdminListDao {

	protected Connection conn;

	public AdminListDao(Connection conn) {
		this.conn = conn;
	}

	public List<AdminListDto> selectAll() throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();

		sb.append("   select");
		sb.append("          OPERATOR_ID");
		sb.append("         ,FIRST_NAME");
		sb.append("         ,LAST_NAME");
		sb.append("         ,PASSWORD");
		sb.append("         ,EMAIL");
		sb.append("         ,OPERATOR_DIVISION");
		sb.append("     from OPERATOR");
		sb.append("   order by OPERATOR_ID");

		List<AdminListDto> list = new ArrayList<>();
		// ステートメントオブジェクトを作成する
		try (Statement stmt = conn.createStatement()) {

			// SQL文を実行する
			ResultSet rs = stmt.executeQuery(sb.toString());
			while (rs.next()) {
				AdminListDto dto = new AdminListDto();
				dto.setOperatorId(rs.getInt("OPERATOR_ID"));
				dto.setFirstName(rs.getString("FIRST_NAME"));
				dto.setLastName(rs.getString("LAST_NAME"));				
				dto.setEmail(rs.getString("EMAIL"));
				dto.setOperatorDivision(rs.getString("OPERATOR_DIVISION"));
				list.add(dto);
			}
		}

		return list;

	}

	public int insert(AdminListDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into OPERATOR");
		sb.append("           (");
		sb.append("             FIRST_NAME");
		sb.append("            ,LAST_NAME");
		sb.append("            ,EMAIL");
		sb.append("            ,OPERATOR_ID");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getFirstName());
			ps.setString(2, dto.getLastName());
			ps.setString(3, dto.getEmail());
			ps.setInt(4, dto.getOperatorId());
			System.out.println(ps.toString());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}
	

	public int delete(AdminListDto dto) throws SQLException{
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from OPERATOR");
		sb.append("       where OPERATOR_ID = ?");
		sb.append("         and UPDATE_NUMBER = ?");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getOperatorId());
			ps.setInt(2, dto.getUpdateNumber());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

}
