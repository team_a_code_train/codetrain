package jp.keronos.dto;


public class CompanyDto {

	/* 法人ID */
	private int companyId;

	/* 法人名 */
	private String companyName;

	/* メールアドレス */
	private String email;



	private int UpdateNumber;

	private int companyListId;

	private int operatorId;

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}


	public int getCompanyListId() {
		return companyListId;
	}

	public void setCompanyListId(int companyListId) {
		this.companyListId = companyListId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getUpdateNumber() {
		return UpdateNumber;
	}

	public void setUpdateNumber(int updateNumber) {
		UpdateNumber = updateNumber;
	}
}