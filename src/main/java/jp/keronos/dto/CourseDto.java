package jp.keronos.dto;

public class CourseDto {

	//コースID
	private int courseId;

	//コース名
	private String courseName;

	//コース概要
	private String courseOverview;

	//前提条件
	private String precondition;

	//ゴール
	private String goal;

	//電車プラン
	private String trainCourse;

	//学習目安時間
	private int studyTime;

	//カリキュラムID
	private int curriculumId;

	//カテゴリーID
	private int categoryId;

	//閲覧フラグ
	private int viewFlg;

	//更新番号
	private int updateNumber;

	//カテゴリ名
	private String categoryName;

	//カリキュラム履歴
	private int curriculumHistry;

	//カリキュラムの数
	private int curriculumCount;

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseOverview() {
		return courseOverview;
	}

	public void setCourseOverview(String courseOverview) {
		this.courseOverview = courseOverview;
	}

	public String getPrecondition() {
		return precondition;
	}

	public void setPrecondition(String precondition) {
		this.precondition = precondition;
	}

	public String getGoal() {
		return goal;
	}

	public void setGoal(String goal) {
		this.goal = goal;
	}

	public String getTrainCourse() {
		return trainCourse;
	}

	public void setTrainCourse(String trainCourse) {
		this.trainCourse = trainCourse;
	}

	public int getStudyTime() {
		return studyTime;
	}

	public void setStudyTime(int studyTime) {
		this.studyTime = studyTime;
	}

	public int getCurriculumId() {
		return curriculumId;
	}

	public void setCurriculumId(int curriculumId) {
		this.curriculumId = curriculumId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getViewFlg() {
		return viewFlg;
	}

	public void setViewFlg(int viewFlg) {
		this.viewFlg = viewFlg;
	}

	public int getUpdateNumber() {
		return updateNumber;
	}

	public void setUpdateNumber(int updateNumber) {
		this.updateNumber = updateNumber;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getCurriculumHistry() {
		return curriculumHistry;
	}

	public void setCurriculumHistry(int curriculumHistry) {
		this.curriculumHistry = curriculumHistry;
	}

	public int getCurriculumCount() {
		return curriculumCount;
	}

	public void setCurriculumCount(int curriculumCount) {
		this.curriculumCount = curriculumCount;
	}



}