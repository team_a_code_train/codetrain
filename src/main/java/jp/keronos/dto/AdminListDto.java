package jp.keronos.dto;

public class AdminListDto {

	private int operatorId;
	
	private String firstName;
	
	private String lastName;
	
	private String password;
	
	private String email;
	
	private String operatorDivision;
	
	private int updateNumber;

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOperatorDivision() {
		return operatorDivision;
	}

	public void setOperatorDivision(String operatorDivision) {
		this.operatorDivision = operatorDivision;
	}

	public int getUpdateNumber() {
		return updateNumber;
	}

	public void setUpdateNumber(int updateNumber) {
		this.updateNumber = updateNumber;
	}
}
