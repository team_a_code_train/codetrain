package jp.keronos.dto;

import java.sql.Timestamp;

public class MessageDto {

	/* メッセージID */
	private int messageId;
	
	/* メッセージ */
	private String message;
	
	/* ユーザID */
	private int userId;
	
	/* 検閲フラグ */
	private boolean viewFlg;
	
	/* 運用担当者ID */
	private int operatorId;
	
	/* 月日 */
	private Timestamp date;
	
	/* 会社ID */
	private int companyId;
	
	/* 更新番号 */
	private int updateNumber;
	
	/* メッセージカテゴリID */
	private int messageCategoryId;
	
	/* メッセージカテゴリ */
	private int messageCategoryName;
	
	private String firstName;
	
	private String lastName;
	
	private String companyName;

	private int userListId;

	public int getMessageId() {
		return messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public boolean isViewFlg() {
		return viewFlg;
	}

	public void setViewFlg(boolean viewFlg) {
		this.viewFlg = viewFlg;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOpearatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getUpdateNumber() {
		return updateNumber;
	}

	public void setUpdateNumber(int updateNumber) {
		this.updateNumber = updateNumber;
	}


	public int getMessageCategoryId() {
		return messageCategoryId;
	}

	public void setMessageCategoryId(int messageCategoryId) {
		this.messageCategoryId = messageCategoryId;
	}

	public int getMessageCategoryName() {
		return messageCategoryName;
	}

	public void setMessageCategoryName(int messageCategoryName) {
		this.messageCategoryName = messageCategoryName;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public int getUserListId() {
		return userListId;
	}

	public void setUserListId(int userListId) {
		this.userListId = userListId;
	}

	
}
