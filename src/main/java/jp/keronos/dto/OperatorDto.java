package jp.keronos.dto;

public class OperatorDto {

	/**
	 * ユーザ情報
	 * @author Mr.X
	 */
	/* ユーザID */
	private int operatorId;
	
	private int operatorListId;
	
	public int getOperatorListId() {
		return operatorListId;
	}

	public void setOperatorListId(int operatorListId) {
		this.operatorListId = operatorListId;
	}

	/* eメール */
	private String email;

	/* パスワード */
	private String password;

	/* 新しいパスワード */
	private String newPassword;

	/* 新しいパスワード */
	private String checkPassword;

	/* 名 */
	private String firstName;

	/* 姓 */
	private String lastName;

	/* 区分 */
	private String operatorDivision;
	

	/* 更新番号 */
	private int updateNumber;
	
	private boolean operatorFlg;

	public boolean isOperatorFlg() {
		return operatorFlg;
	}

	public void setOperatorFlg(boolean operatorFlg) {
		this.operatorFlg = operatorFlg;
	}

	public int getOperatorId() {
		return operatorId;
	}


	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getCheckPassword() {
		return checkPassword;
	}

	public void setCheckPassword(String checkPassword) {
		this.checkPassword = checkPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOperatorDivision() {
		return operatorDivision;
	}

	public void setOperatorDivision(String operatorDivision) {
		this.operatorDivision = operatorDivision;
	}

	public int getUpdateNumber() {
		return updateNumber;
	}

	public void setUpdateNumber(int updateNumber) {
		this.updateNumber = updateNumber;
	}



}
