package jp.keronos.dto;

import java.sql.Timestamp;

public class UserDto {

	/**
	 * ユーザ情報
	 * @author Mr.X
	 */
	/* ユーザID */
	private int userId;

	/* eメール */
	private String email;

	/* パスワード */
	private String password;

	private String password1;

	private String password2;

	private String password3;

	/* 名 */
	private String firstName;

	/* 姓 */
	private String lastName;

	/* 会社ID */
	private int companyId;

	/* 会社名 */
	private String companyName;

	/* スキル */
	private String skill;

	/* 凍結理由 */
	private String reason;

	/* 凍結フラグ */
	private Boolean status_flg;

	/* 凍結日時 */
	private Timestamp freeze_date;

	/* 更新番号 */
	private int updateNumber;

	/* 削除フラグ */
	private Boolean del_flg;

	/* 削除日時 */
	private Timestamp del_date;

	private int userIdCount;

	private int status_flgCount;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getStatus_flg() {
		return status_flg;
	}

	public void setStatus_flg(Boolean status_flg) {
		this.status_flg = status_flg;
	}

	public Timestamp getFreeze_date() {
		return freeze_date;
	}

	public void setFreeze_date(Timestamp freeze_date) {
		this.freeze_date = freeze_date;
	}

	public int getUpdateNumber() {
		return updateNumber;
	}

	public void setUpdateNumber(int updateNumber) {
		this.updateNumber = updateNumber;
	}

	public Boolean getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(Boolean del_flg) {
		this.del_flg = del_flg;
	}

	public Timestamp getDel_date() {
		return del_date;
	}

	public void setDel_date(Timestamp del_date) {
		this.del_date = del_date;
	}

	public String getPassword1() {
		return password1;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public String getPassword3() {
		return password3;
	}

	public void setPassword3(String password3) {
		this.password3 = password3;
	}

	public int getUserIdCount() {
		return userIdCount;
	}

	public void setUserIdCount(int userIdCount) {
		this.userIdCount = userIdCount;
	}

	public int getStatus_flgCount() {
		return status_flgCount;
	}

	public void setStatus_flgCount(int status_flgCount) {
		this.status_flgCount = status_flgCount;
	}



}
