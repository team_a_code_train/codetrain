package jp.keronos.dto;

public class CorporationUserDto {
	/**
	 * ユーザ情報
	 * @author Mr.X
	 */
	/* ユーザID */
	private int userId;

	/* eメール */
	private String email;

	/* パスワード */
	private String password;

	/* 新しいパスワード */
	private String newPassword;

	/* 確認用パスワード */
	private String checkPassword;

	/* 名 */
	private String firstName;

	/* 姓 */
	private String lastName;

	/* 会社ID */
	private int companyId;

	/* 会社名 */
	private String companyName;

	/* 請求ID */
	private int invoiceId;

	/* 更新番号 */
	private int updateNumber;

	/* リストユーザーID */
	private int corporationUserListId;

	/* オペレーターID */
	private int operatorId;

	/**
	 * ユーザIDを取得する
	 * @return ユーザID
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * ユーザIDを設定する
	 * @param userId ユーザID
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * eメールを取得する
	 * @return eメール
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * eメールを設定する
	 * @param email eメール
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * パスワードを取得する
	 * @return パスワード
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * パスワードを設定する
	 * @param password パスワード
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 新しいパスワードを取得する
	 * @return 新しいパスワード
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * 新しいパスワードを設定する
	 * @param newPassword 新しいパスワード
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * 新しいパスワードを取得する
	 * @return 新しいパスワード
	 */
	public String getCheckPassword() {
		return checkPassword;
	}

	/**
	 * 確認用パスワードを設定する
	 * @param newPassword 確認用パスワード
	 */
	public void setCheckPassword(String checkPassword) {
		this.checkPassword = checkPassword;
	}

	/**
	 * 名を取得する
	 * @return 名
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * 名を設定する
	 * @param firstName 名
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * 姓を取得する
	 * @return 姓
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * 姓を設定する
	 * @param lastName 姓
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * 会社IDを取得する
	 * @return 会社ID
	 */
	public int getCompanyId() {
		return companyId;
	}

	/**
	 * 会社IDを設定する
	 * @param companyId 会社ID
	 */
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	/**
	 * 会社名を取得する
	 * @return 会社名
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * 会社名を設定する
	 * @param companyName 会社名
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * 請求IDを取得する
	 * @return 請求ID
	 */
	public int getInvoiceId() {
		return invoiceId;
	}

	/**
	 * 請求IDを設定する
	 * @param invoiceId 請求ID
	 */
	public void setInvoiceId(int invoiceId) {
		this.invoiceId = invoiceId;
	}

	/**
	 * 更新番号を取得する
	 * @return 更新番号
	 */
	public int getUpdateNumber() {
		return updateNumber;
	}

	/**
	 * 更新番号を設定する
	 * @param updateNumber 更新番号
	 */
	public void setUpdateNumber(int updateNumber) {
		this.updateNumber = updateNumber;
	}

	public int getCorporationUserListId() {
		return corporationUserListId;
	}

	public void setCorporationUserListId(int corporationUserListId) {
		this.corporationUserListId = corporationUserListId;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

}
