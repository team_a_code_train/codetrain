package jp.keronos.dto;

public class CategoryDto {
    
    private int categoryId;
    
    private String categoryName;
    
    private String categoryOverview;
    
    private int updateNumber;
    
    public int getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
    public String getCategoryName() {
        return categoryName;
    }
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    public int getUpdateNumber() {
        return updateNumber;
    }
    public void setUpdateNumber(int updateNumber) {
        this.updateNumber = updateNumber;
    }
	public String getCategoryOverview() {
		return categoryOverview;
	}
	public void setCategoryOverview(String categoryOverView) {
		this.categoryOverview = categoryOverView;
	}
    
}

