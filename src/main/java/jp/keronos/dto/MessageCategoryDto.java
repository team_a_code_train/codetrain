package jp.keronos.dto;

public class MessageCategoryDto {

	/* カテゴリーメッセージID */
	private int messageCategoryId;
	
	/* カテゴリーメッセージ */
	private String messageCategoryName;

	public int getMessageCategoryId() {
		return messageCategoryId;
	}

	public void setMessageCategoryId(int messageCategoryId) {
		this.messageCategoryId = messageCategoryId;
	}

	public String getMessageCategoryName() {
		return messageCategoryName;
	}

	public void setMessageCategoryName(String messageCategoryName) {
		this.messageCategoryName = messageCategoryName;
	}
	
}
