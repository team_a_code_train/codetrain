package jp.keronos.dto;

public class HistoryDto {

	//コースIDカウント
	private int courseIdCount;
	
	private int historyId;
	
	private int userId;
	
	private int courseId;
	
	private int categoryId;

	public int getCourseIdCount() {
		return courseIdCount;
	}

	public void setCourseIdCount(int courseIdCount) {
		this.courseIdCount = courseIdCount;
	}

	public int getHistoryId() {
		return historyId;
	}

	public void setHistoryId(int historyId) {
		this.historyId = historyId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

}
