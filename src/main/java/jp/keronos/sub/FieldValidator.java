package jp.keronos.sub;

import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.CorporationUserDto;
import jp.keronos.dto.UserDto;


public class FieldValidator {

	/**
	 * 入力チェック
	 * @param dto チャンネル情報
	 * @return エラーメッセージ
	 */
	public static List<String> corporationUserPasswordValidation(CorporationUserDto dto) {
		List<String> errorMessageList = new ArrayList<>();

		// 各種パスワードが未入力の場合
		if ("".equals(dto.getPassword())) {
			errorMessageList.add("現在のパスワードを入力してください");
		}

		if ("".equals(dto.getNewPassword())) {
			errorMessageList.add("新しいパスワードを入力してください");
		}

		if ("".equals(dto.getCheckPassword())) {
			errorMessageList.add("確認用パスワードを入力してください");
		}

		return errorMessageList;
	}


	public static List<String> corporationUserValidation(CorporationUserDto dto) {
		List<String> errorMessageList = new ArrayList<>();



		if ("".equals(dto.getEmail())) {
			errorMessageList.add("アドレスを入力してください");
		}

		if ("".equals(dto.getLastName())) {
			errorMessageList.add("姓を入力してください");
		}

		if ("".equals(dto.getFirstName())) {
			errorMessageList.add("名を入力してください");
		}

		if ("".equals(dto.getCompanyName())) {
			errorMessageList.add("企業名を入力してください");
		}

		return errorMessageList;
	}

	public static List<String> reasonValidation(UserDto dto) {
		List<String> errorMessageList = new ArrayList<>();

		if ("".equals(dto.getReason())) {
			errorMessageList.add("凍結理由を入力してください");
		}

		if (dto.getReason().length() > 40) {
			errorMessageList.add("凍結理由は40文字以内で入力してください");
		}

		return errorMessageList;
	}

	public static List<String> userValidation(UserDto dto) {
		List<String> errorMessageList = new ArrayList<>();

		if ("".equals(dto.getEmail())) {
			errorMessageList.add("メールアドレスを入力してください");
		}

		if ("".equals(dto.getFirstName())) {
			errorMessageList.add("姓を入力してください");
		}

		if ("".equals(dto.getLastName())) {
			errorMessageList.add("名を入力してください");
		}

		return errorMessageList;
	}
}

