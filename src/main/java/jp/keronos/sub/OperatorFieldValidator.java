package jp.keronos.sub;

import java.util.ArrayList;


import java.util.List;

import jp.keronos.dto.OperatorDto;

public class OperatorFieldValidator {

	/**
	 * 入力チェック
	 * @param dto チャンネル情報
	 * @return エラーメッセージ
	 */
	
	public static List<String> operatorValidation(OperatorDto dto) {
	List<String> errorMessageList = new ArrayList<>();
		
		if ("".equals(dto.getFirstName())) {
			errorMessageList.add("姓を入力してください");
		}
		
		if ("".equals(dto.getLastName())) {
			errorMessageList.add("名を入力してください");
		}
		
		if (dto.getFirstName().length() > 30) {
			errorMessageList.add("姓は30文字以内で入力してください");
		}
		
		if (dto.getLastName().length() > 30) {
			errorMessageList.add("名は30文字以内で入力してください");
		}
		
		
		if ("".equals(dto.getEmail())) {
			errorMessageList.add("メールアドレスを入力してください");
		}
		
		if (dto.getEmail().length() > 200) {
			errorMessageList.add("メールアドレスは200文字以内で入力してください");
		}
		
		return errorMessageList;
	}
}
