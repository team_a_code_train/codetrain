package jp.keronos.sub;

import java.util.ArrayList;
import java.util.List;

import jp.keronos.dto.MessageDto;

public class MessageUtility {
	/**
	 * 入力チェック
	 * @param dto 入力情報
	 * @return エラーメッセージ
	 */
	public static List<String> validation(MessageDto dto) {
		List<String> errorMessageList = new ArrayList<>();
		
		if ("".equals(dto.getMessage())) {
			errorMessageList.add("メッセージを入力してください");
		}
		
		return errorMessageList;
	}
}
