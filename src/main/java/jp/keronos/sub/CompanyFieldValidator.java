package jp.keronos.sub;

import java.util.ArrayList;


import java.util.List;

import jp.keronos.dto.CompanyDto;

public class CompanyFieldValidator {

	public static List<String> companyValidation(CompanyDto dto) {
	List<String> errorMessageList = new ArrayList<>();
		
		if ("".equals(dto.getCompanyName())) {
			errorMessageList.add("企業名を入力してください");
		}
		
		if (dto.getCompanyName().length() > 30) {
			errorMessageList.add("企業名は30文字以内で入力してください");
		}
				
		
		if ("".equals(dto.getEmail())) {
			errorMessageList.add("メールアドレスを入力してください");
		}
		
		if (dto.getEmail().length() > 200) {
			errorMessageList.add("メールアドレスは200文字以内で入力してください");
		}
		return errorMessageList;
	}
}
