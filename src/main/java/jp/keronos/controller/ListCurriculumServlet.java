package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CourseListDao;
import jp.keronos.dao.ListCurriculumDao;
import jp.keronos.dto.CourseDto;
import jp.keronos.dto.ListCurriculumDto;
import jp.keronos.sub.DataSourceManager;

/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/list-curriculum")
public class ListCurriculumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ListCourseServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		//String message = request.getParameter("list-knowledge");

		//PrintWriter out = response.getWriter();
		//out.println(message);

		//doGet
		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			// セッションを取得する
			HttpSession session = request.getSession(false);
			session.removeAttribute("queries");


			//コース一覧情報を取得
			//CourseListDao CListdao = new CourseListDao(conn);
			//List<CourseDto> CourseList = CListdao.selectAll();
			
			//コースIDを取得する
			int courseId = Integer.parseInt(request.getParameter("courseId"));

			//コースIDに該当するコース情報を取得する（コース詳細上部のコース情報）
			CourseListDao courseListDao = new CourseListDao(conn);
			List<CourseDto> courseList = courseListDao.selectByCourseId(courseId);

			//２、コースIDに該当するカリキュラム情報を取得する（コース詳細のカリキュラム表示するためのやつ）
			ListCurriculumDao dao = new ListCurriculumDao(conn);
			List<ListCurriculumDto> curriculumlist = dao.selectByCourseId(courseId);

			//リクエストスコープにコースIDに該当するコース情報を保持する
			//リクエストスコープにカリキュラム情報を保持する
			//request.setAttribute("courseId", courseId);
			request.setAttribute("curriculumlist", curriculumlist);
			request.setAttribute("courseList", courseList);


			//URIをリクエストスコープに保持する
			request.setAttribute("uri", request.getRequestURI());


			//セッションスコープ内のナブバーメッセージをリクエストスコープに保持し、セッションスコープ内からナブバーメッセージを削除する
			session.setAttribute("navbarmessage", request.getAttribute("navbarmessage"));
			request.removeAttribute("navbarmessage");

			//list-curriculum.jspに転送する
			request.getRequestDispatcher("/WEB-INF/list-curriculum.jsp").forward(request, response);
		}

		  catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}


	//doPost
	//deGetを呼び出す

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}