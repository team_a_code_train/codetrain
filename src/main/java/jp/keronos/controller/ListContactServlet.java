package jp.keronos.controller;

import java.io.IOException;




import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.keronos.dao.MessageDao;
import jp.keronos.dao.OperatorDao;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import jp.keronos.dto.MessageDto;
import jp.keronos.dto.OperatorDto;
import jp.keronos.sub.DataSourceManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




@WebServlet("/list-contact")
public class ListContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private Logger logger = LoggerFactory.getLogger(ListContactServlet.class);
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");
		
		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			
			if (session == null || session.getAttribute("operator") == null) {
				logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
				// ログイン画面に遷移する
				request.getRequestDispatcher("operator-login").forward(request, response);
				return;
			}

			MessageDao dao = new MessageDao(conn);
			List<MessageDto> list = dao.selectUserList();
			
			// 利用者情報一覧データをリクエストに保持する
			request.setAttribute("list", list);
			
			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI());
			
			// メッセージをリクエストに保持する
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");

			// 連絡事項一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-contact.jsp").forward(request, response);
			
		} catch (SQLException | NamingException e) {
			
			logger.error("{} {}", e.getClass(), e.getMessage());
			
			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
