package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.sub.DataSourceManager;
import jp.keronos.dao.MessageDao;
import jp.keronos.dto.MessageDto;
import jp.keronos.dto.OperatorDto;
import jp.keronos.dto.UserDto;

/**
 * Servlet implementation class ThredDetailServlet
 */
@WebServlet("/list-operator-message")
public class ListOperatorMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ListOperatorMessageServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("operator") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("operator-menu.jsp").forward(request, response);
			return;
		}

		// ログインユーザ情報を取得する
		OperatorDto operator = (OperatorDto) session.getAttribute("operator");
		
		
		
		// フォームのデータを取得する
		MessageDto dto = new MessageDto();
		request.setCharacterEncoding("UTF-8");
		int uId = Integer.parseInt(request.getParameter("userId"));
		UserDto uDto = new UserDto();
		uDto.setUserId(uId);		
		session.setAttribute("uDto", uDto);
		
		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			//HttpSession session = request.getSession(false);

			//メッセージリストを取得する
			MessageDao dao = new MessageDao(conn);
			List<MessageDto> operatorMessageList = new ArrayList<MessageDto>();
			operatorMessageList = dao.selectByUserIdFromOperator(uId);
			
			// メッセージリストをリクエストに保持する
			//request.setAttribute("messageList", messageList);

			// メッセージをリクエストに保持する
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");

			// ナブバーメッセージをリクエストに保持する
			request.setAttribute("navbarMessage", session.getAttribute("navbarMessage"));
			session.removeAttribute("navbarMessage");

			request.setAttribute("operatorMessageList", operatorMessageList);
			
			// メッセージ一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-operator-message.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
