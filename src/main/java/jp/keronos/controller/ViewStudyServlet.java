package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CourseListDao;
import jp.keronos.dao.ListCurriculumDao;
import jp.keronos.dto.CourseDto;
import jp.keronos.dto.ListCurriculumDto;
import jp.keronos.sub.DataSourceManager;

/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/study")
public class ViewStudyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ViewStudyServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		int curriculumId = Integer.parseInt(request.getParameter("curriculumId"));
		
		// コネクションを取得
		try (Connection conn = DataSourceManager.getConnection()) {
			// セッションを取得する
			HttpSession session = request.getSession(true);
			session.removeAttribute("queries");
			
			//カリキュラム情報取得する
			ListCurriculumDao dao = new ListCurriculumDao(conn);
			List<ListCurriculumDto> studyList = dao.selectByCurriculumId(curriculumId);

			//リクエストスコープにカリキュラム情報を保持する
			request.setAttribute("studyList", studyList);

			//URIをリクエストスコープに保持する
			request.setAttribute("uri", request.getRequestURI());


			//セッションスコープ内のナブバーメッセージをリクエストスコープに保持し、セッションスコープ内からナブバーメッセージを削除する
			session.setAttribute("navbarmessage", request.getAttribute("navbarmessage"));
			request.removeAttribute("navbarmessage");

			//study.jspに転送する
			request.getRequestDispatcher("/WEB-INF/study.jsp").forward(request, response);
		}

		  catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}


	//doPost
	//deGetを呼び出す

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}