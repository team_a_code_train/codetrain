package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.sub.DataSourceManager;
import jp.keronos.sub.MessageUtility;
import jp.keronos.dao.HistoryDao;
import jp.keronos.dto.HistoryDto;
import jp.keronos.dto.UserDto;

/**
 * Servlet implementation class AddMessageServlet
 */
@WebServlet("/add-history")
public class AddHistoryServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddHistoryServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// 遷移する
		request.getRequestDispatcher("WEB-INF/add-history.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// ログインユーザ情報を取得する
		UserDto user = (UserDto) session.getAttribute("user");

		// フォームのデータを取得する
		HistoryDto dto = new HistoryDto();
		dto.setCourseId(Integer.parseInt(request.getParameter("courseId")));
		dto.setCategoryId(Integer.parseInt(request.getParameter("categoryId")));
		dto.setUserId(user.getUserId());

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// メッセー情報を追加する
			HistoryDao historyDao = new HistoryDao(conn);
			historyDao.insert(dto);

			session.setAttribute("message", "学習しました");

			// ナレッジ一覧画面に遷移する
			response.sendRedirect("list-course?categoryId=" + request.getParameter("categoryId"));

		} catch (SQLException | NamingException e) {
			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}
}
