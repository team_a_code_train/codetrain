package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CorporationUserDao;
import jp.keronos.dto.CorporationUserDto;
import jp.keronos.sub.DataSourceManager;

@WebServlet("/change-corporation-password")
public class ChangeCorporationPasswordServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ChangeCorporationPasswordServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(true);

		if (session == null || session.getAttribute("corporationUser") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// ログイン画面に遷移する
			request.getRequestDispatcher("corporation-login").forward(request, response);
			return;
		}

		// メッセージをリクエストに保持する
		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
		session.removeAttribute("errorMessageList");

		// パスワード変更画面に遷移する
		request.getRequestDispatcher("WEB-INF/change-corporation-password.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションを取得する
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("corporationUser") == null) {
			// ログイン画面に遷移する
			request.getRequestDispatcher("corporation-login").forward(request, response);
			return;
		}

		// ログインユーザ情報を取得する
		CorporationUserDto user = (CorporationUserDto)session.getAttribute("corporationUser");

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("corporationLoginPassword");
		String newPassword = request.getParameter("corporationNewPassword");
		String checkPassword = request.getParameter("corporationCheckPassword");
		String clId = user.getEmail();

		// ログインID、パスワードが未入力の場合
		if ("".equals(password) || "".equals(newPassword) || "".equals(newPassword)) {

			session.setAttribute("errorMessageList", "パスワードを入力してください");
			request.getRequestDispatcher("WEB-INF/change-corporation-password.jsp").forward(request, response);
		}

		CorporationUserDto dto = new CorporationUserDto();

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 接続情報を取得する
			CorporationUserDao dao = new CorporationUserDao(conn);

			CorporationUserDto currentAccount = dao.findByIdAndPassword(clId, password);
			CorporationUserDto newAccount = dao.findByIdAndPassword(clId, newPassword);

			// 現在のパスワードの比較と、新しいパスワードが確認用と一致するか確認
			if (currentAccount == null) {
				session.setAttribute("errorMessageList", "現在のパスワードが正しくありません");
				// パスワード変更画面に戻る
				response.sendRedirect("change-corporation-password");
				return;
			} else if ("".equals(newPassword) || !(newPassword).equals(checkPassword)) {
				session.setAttribute("errorMessageList", "新しいパスワードと確認用のパスワードが正しくありません");
				// パスワード変更画面に戻る
				response.sendRedirect("change-corporation-password");
				return;
			} else if (newAccount != null) {
				session.setAttribute("errorMessageList", "パスワードが変更されていません");
				// パスワード変更画面に戻る
				response.sendRedirect("change-corporation-password");
				return;
			}

			// パスワード変更情報をまとめる
			dto.setPassword(newPassword);
			dto.setUserId(user.getUserId());
			dto.setUpdateNumber(user.getUpdateNumber());

			dao.update(dto);

			// 更新通知をセッションスコープに保持する
			session.setAttribute("message", "窓口ユーザー情報を更新しました");

			// 利用者一覧画面に遷移する
			response.sendRedirect("list-user");

		} catch (SQLException | NamingException e) {

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}
}
