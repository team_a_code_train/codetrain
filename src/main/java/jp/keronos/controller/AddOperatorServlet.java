package jp.keronos.controller;

import java.io.IOException;




import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.sub.OperatorFieldValidator;
import jp.keronos.dto.OperatorDto;
import jp.keronos.sub.DataSourceManager;
import jp.keronos.dao.OperatorDao;



/**
 * Servlet implementation class AddAdminServlet
 */
@WebServlet("/add-operator")
public class AddOperatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddOperatorServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		
		//運営担当者一覧に遷移する
		response.sendRedirect("list-operator.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		
		if (session == null || session.getAttribute("operator") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			
			// 運営担当者一覧画面に遷移する
			request.getRequestDispatcher("list-operator").forward(request, response);
			return;
		}
		
		// セッションからログインユーザ情報を取得する
		OperatorDto operator = (OperatorDto)session.getAttribute("operator");
		
		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		OperatorDto dto = new OperatorDto();
		dto.setFirstName(request.getParameter("firstName"));
		dto.setLastName(request.getParameter("lastName"));
		dto.setEmail(request.getParameter("email"));
		dto.setOperatorDivision(request.getParameter("operatorDivision"));
		dto.setOperatorId(operator.getOperatorId());
	
		// 入力チェック
		List<String> errorMessageList = OperatorFieldValidator.operatorValidation(dto);
		if (errorMessageList.size() != 0) {
			// チャンネル登録画面に遷移する
			request.setAttribute("errorMessageList", errorMessageList);
			request.getRequestDispatcher("form-operator").forward(request, response);
			return;
		}
		
		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			
			// 運営担当者を追加する
			OperatorDao dao = new OperatorDao(conn);
			dao.insert(dto);

			// 姓、名をリクエストスコープに保持する
			session.setAttribute("message", dto.getLastName() + dto.getFirstName() + "を登録しました");
			
			// 運営担当者一覧画面に遷移する
			response.sendRedirect("list-operator");
			
		} catch (SQLException | NamingException e) {
			
			logger.error("{} {}", e.getClass(), e.getMessage());
			e.printStackTrace();
			
			// 氏名が重複している場合
			if (e.getMessage().contains("Duplicate entry")) {
				// エラーメッセージをリクエストスコープに保持する
				
				errorMessageList.add("そのアカウントは既に存在します");
				request.setAttribute("errorMessageList", errorMessageList);
				
				// フォームのデータをリクエストスコープに保持する
				request.setAttribute("data", dto);

				// 運営担当者登録画面に遷移する
				request.getRequestDispatcher("form-operator").forward(request, response);
			} else {
				// システムエラー画面に遷移する
				request.getRequestDispatcher("system-error.jsp").forward(request, response);
			}
		}
	}
}
