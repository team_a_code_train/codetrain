package jp.keronos.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class FormChannelServlet
 */
@WebServlet("/form-operator")
public class FormOperatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//private Logger logger = LoggerFactory.getLogger(FormAdminServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// 追加モード
		request.setAttribute("isAdd", true);
		
		// 運営担当者登録画面に遷移する
		request.getRequestDispatcher("WEB-INF/view-operator.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}
