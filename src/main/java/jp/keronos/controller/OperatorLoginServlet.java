package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.OperatorDao;
import jp.keronos.dto.OperatorDto;
import jp.keronos.sub.DataSourceManager;

@WebServlet(urlPatterns={"/operator-login"}, initParams={@WebInitParam(name="operatorPassword", value="CodeTrain123")})
public class OperatorLoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(OperatorLoginServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// ログイン画面に遷移する
		request.getRequestDispatcher("WEB-INF/operator-login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// フォームのデータを取得する
		String operatorLoginId = request.getParameter("operatorLoginId");
		String operatorLoginPassword = request.getParameter("operatorLoginPassword");

		// セッションを取得する
		HttpSession session = request.getSession(true);

		// ログインID、パスワードが未入力の場合
		if ("".equals(operatorLoginId) || "".equals(operatorLoginPassword)) {
			logger.warn("ログイン失敗 {}", request.getRemoteAddr());

			session.setAttribute("Message", "メールアドレス、パスワードを入力してください");
		}

		
		//a
		try (Connection conn = DataSourceManager.getConnection()) {

			// ログイン処理
			OperatorDao loginDao = new OperatorDao(conn);
			OperatorDto operatorDto =
					loginDao.findByIdAndPassword(operatorLoginId, operatorLoginPassword);

			session.setAttribute("operator", operatorDto);
			session.removeAttribute("Message");

			// ログイン失敗時
			if (operatorDto == null) {
				logger.warn("ログイン失敗 {} mail={} pass={}",
						request.getRemoteAddr(), operatorLoginId, operatorLoginPassword);
				session.setAttribute("Message", "メールアドレスまたはパスワードが間違っています");
				
			}

			// 初回ログイン時
			if (getInitParameter("operatorPassword").equals(operatorLoginPassword)) {
				request.getRequestDispatcher("WEB-INF/change-operator-password.jsp").forward(request, response);

				return;
			}

			// ログイン成功後は利用者一覧画面に遷移する
			response.sendRedirect("operator-menu");



		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			e.printStackTrace();

			// システムエラーに遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}
}
