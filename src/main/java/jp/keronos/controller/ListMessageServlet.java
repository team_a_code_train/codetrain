package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.sub.DataSourceManager;
import jp.keronos.dao.MessageDao;
import jp.keronos.dto.MessageDto;
import jp.keronos.dto.UserDto;

/**
 * Servlet implementation class ThredDetailServlet
 */
@WebServlet("/list-message")
public class ListMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ListMessageServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// ユーザIDを取得する
		// int userId = Integer.parseInt(request.getParameter("userId"));
		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// ログインユーザ情報を取得する
		UserDto user = (UserDto) session.getAttribute("user");
		int userId = user.getUserId();
		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			//HttpSession session = request.getSession(false);

			// メッセージリストを取得する
			MessageDao MessageDao = new MessageDao(conn);
			List<MessageDto> messageList = MessageDao.selectByUserId(userId);

			// メッセージリストをリクエストに保持する
			request.setAttribute("messageList", messageList);

			// URIをリクエストに保持する??
			// request.setAttribute("uri",
			// request.getRequestURI().concat("?").concat(request.getQueryString()));

			// メッセージをリクエストに保持する
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");

			// ナブバーメッセージをリクエストに保持する
			request.setAttribute("navbarMessage", session.getAttribute("navbarMessage"));
			session.removeAttribute("navbarMessage");

			// メッセージ一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-message.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
