package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.OperatorDao;
import jp.keronos.dto.OperatorDto;
import jp.keronos.sub.DataSourceManager;

@WebServlet("/change-operator-password")
public class ChangeOperatorPasswordServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ChangeOperatorPasswordServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(true);

		if (session == null || session.getAttribute("operator") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// ログイン画面に遷移する
			request.getRequestDispatcher("operator-login").forward(request, response);
			return;
		}

		// メッセージをリクエストに保持する
		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
		session.removeAttribute("errorMessageList");

		// パスワード変更画面に遷移する
		request.getRequestDispatcher("WEB-INF/change-operator-password.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションを取得する
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("operator") == null) {
			// ログイン画面に遷移する
			request.getRequestDispatcher("operator-login").forward(request, response);
			return;
		}

		// ログインユーザ情報を取得する
		OperatorDto operator = (OperatorDto)session.getAttribute("operator");

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("operatorLoginPassword");
		String newPassword = request.getParameter("operatorNewPassword");
		String checkPassword = request.getParameter("operatorCheckPassword");
		String olId = operator.getEmail();

		OperatorDto dto = new OperatorDto();

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 接続情報を取得する
			OperatorDao dao = new OperatorDao(conn);

			OperatorDto currentAccount = dao.findByIdAndPassword(olId, password);
			OperatorDto newAccount = dao.findByIdAndPassword(olId, newPassword);

			// 現在のパスワードの比較と、新しいパスワードが確認用と一致するか確認
			if (currentAccount == null) {
				session.setAttribute("errorMessage", "現在のパスワードが正しくありません");
				// パスワード変更画面に戻る
				response.sendRedirect("change-operator-password");
				return;
			} else if ("".equals(newPassword) || !(newPassword).equals(checkPassword)) {
				session.setAttribute("errorMessage", "新しいパスワードと確認用のパスワードが正しくありません");
				// パスワード変更画面に戻る
				response.sendRedirect("change-operator-password");
				return;
			} else if (newAccount != null) {
				session.setAttribute("errorMessage", "パスワードが変更されていません");
				// パスワード変更画面に戻る
				response.sendRedirect("change-operator-password");
				return;
			}

			// パスワード変更情報をまとめる
			dto.setPassword(newPassword);
			dto.setOperatorId(operator.getOperatorId());
			dto.setUpdateNumber(operator.getUpdateNumber());

			dao.updatePassword(dto);

			// 更新通知をセッションスコープに保持する
			session.setAttribute("message", "管理者情報を更新しました");

			// メニュー画面に遷移する
			request.getRequestDispatcher("operator-menu").forward(request, response);

		} catch (SQLException | NamingException e) {

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}
}
