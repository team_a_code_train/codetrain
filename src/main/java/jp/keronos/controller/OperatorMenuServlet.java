package jp.keronos.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/operator-menu")
public class OperatorMenuServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(CorporationLoginServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");

		if (session == null || session.getAttribute("operator") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());

		// ログイン画面に遷移する
		request.getRequestDispatcher("operator-login").forward(request, response);
		return;
		}
		// メッセージをリクエストに保持する
//		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
//		session.removeAttribute("errorMessageList");


		// メニュー画面に遷移する
		request.getRequestDispatcher("WEB-INF/operator-menu.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}