package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.InvoiceDao;
import jp.keronos.dto.InvoiceDto;
import jp.keronos.sub.DataSourceManager;

@WebServlet("/invoice-month")
public class InvoiceMonthServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(InvoiceMonthServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");

		if (session == null || session.getAttribute("corporationUser") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// ログイン画面に遷移する
			request.getRequestDispatcher("corporation-login").forward(request, response);
			return;
		}

		// 会社IDを取得する
		int companyId = Integer.parseInt(request.getParameter("companyId"));

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 請求書の月情報を取得する
			InvoiceDao invoiceDao = new InvoiceDao(conn);
			List<InvoiceDto> monthList = invoiceDao.selectAllMonthBycompanyId(companyId);

			// 会社IDと月情報をリクエストスコープに保持する
			request.setAttribute("companyId", companyId);
			request.setAttribute("monthList", monthList);
			// 請求月一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/invoice-month.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			e.printStackTrace();

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
