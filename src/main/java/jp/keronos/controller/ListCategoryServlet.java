package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.sub.DataSourceManager;
import jp.keronos.dao.CategoryDao;
import jp.keronos.dto.CategoryDto;

/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/list-category")
public class ListCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private Logger logger = LoggerFactory.getLogger(ListCategoryServlet.class);
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			// セッションを取得する
			HttpSession session = request.getSession(true);
			session.removeAttribute("queries");
			
			// チャンネル一覧を取得する
			CategoryDao categoryDao = new CategoryDao(conn);
			List<CategoryDto> categoryList = categoryDao.selectAll();
			
			// チャンネル一覧データをリクエストに保持する
			request.setAttribute("categoryList", categoryList);
			
			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI());
			
			// メッセージをリクエストに保持する
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");
			
			// ナブバーメッセージをリクエストに保持する
			request.setAttribute("navbarMessage", session.getAttribute("navbarMessage"));
			session.removeAttribute("navbarMessage");

			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-category.jsp").forward(request, response);
			
		} catch (SQLException | NamingException e) {
			
			logger.error("{} {}", e.getClass(), e.getMessage());
			
			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
