package jp.keronos.controller;

import java.io.IOException;



import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dto.OperatorDto;
import jp.keronos.dao.OperatorDao;
import jp.keronos.sub.DataSourceManager;


/**
 * Servlet implementation class DeleteChannelServlet
 */
@WebServlet("/delete-operator")
public class DeleteOperatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(DeleteOperatorServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 運用担当者一覧に遷移する
		response.sendRedirect("list-operator.jsp");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("operator") == null) {
			
			logger.warn("セッションタイムアウト");
			// 運営担当者一覧画面に遷移する
			request.getRequestDispatcher("list-operator").forward(request, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		OperatorDto dto = new OperatorDto();
		dto.setOperatorId(Integer.parseInt(request.getParameter("operatorId")));
		dto.setUpdateNumber(Integer.parseInt(request.getParameter("updateNumber")));

		// コネクションを取得する
		String firstName = "";
		String lastName = "";
		try (Connection conn = DataSourceManager.getConnection()) {

			try {
				// トランザクションを開始する
				conn.setAutoCommit(false);

				// 運営担当者情報を削除する
				OperatorDao operatorDao = new OperatorDao(conn);
				//operatorDao.deleteByOperatorId(dto.getOperatorId());
				
				
				firstName = operatorDao.selectByOperatorId(dto).getFirstName();
				lastName = operatorDao.selectByOperatorId(dto).getLastName();
				
				operatorDao.deleteByOperatorId(dto.getOperatorId());
						
				session.setAttribute("message","担当者を削除しました");
						
			} catch (SQLException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());
				
				// ロールバックする
				conn.rollback();
				throw e;
			} finally {
				conn.setAutoCommit(true);
			}

			// 運営担当者一覧画面に遷移する
			response.sendRedirect("list-operator");
			
		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());
			
			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
			
		}
	}
}
