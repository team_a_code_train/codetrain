package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.InvoiceDao;
import jp.keronos.dto.InvoiceDto;
import jp.keronos.sub.DataSourceManager;

@WebServlet("/invoice-detail")
public class InvoiceDetailServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(InvoiceDetailServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.sendRedirect("corporation-login");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");

		if (session == null || session.getAttribute("corporationUser") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// ログイン画面に遷移する
			request.getRequestDispatcher("corporation-login").forward(request, response);
			return;
		}

		// フォームの会社IDを取得する
		int companyId = Integer.parseInt(request.getParameter("companyId"));

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// フォームの日付情報を取得する
			SimpleDateFormat sdf  = new SimpleDateFormat("yyyy/MM/DD hh:mm:dd");
			String str = request.getParameter("date");
			Date date = sdf.parse(str);
			Timestamp stDate = new Timestamp(date.getTime());

			// 請求書の月情報を取得する
			InvoiceDao invoiceDao = new InvoiceDao(conn);
			InvoiceDto invoice = invoiceDao.findByCompanyIdAndDate(companyId, stDate);

			request.setAttribute("invoice", invoice);
			// 請求詳細画面に遷移する
			request.getRequestDispatcher("WEB-INF/invoice-detail.jsp").forward(request, response);

		} catch (SQLException | NamingException | ParseException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			e.printStackTrace();

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}
}