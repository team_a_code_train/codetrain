package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.keronos.dao.CorporationUserDao;
import jp.keronos.dto.CorporationUserDto;
import jp.keronos.sub.DataSourceManager;



/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/list-corporation-user")
public class ListCorporationUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//private Logger logger = LoggerFactory.getLogger(ListCompanyServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			 //セッションを取得する
			HttpSession session = request.getSession(true);
			session.removeAttribute("queries");

			// 企業アカウントIDを取得する
			String companyId = request.getParameter("companyId");
			int cId = Integer.parseInt(companyId);

			//法人一覧を取得する
			CorporationUserDao dao = new CorporationUserDao(conn);
			List <CorporationUserDto> list = dao.selectByCompanyId(cId);

			//法人一覧データをリクエストに保持する
			request.setAttribute("list", list);

			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI());

			// メッセージをリクエストに保持する
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");

			// ナブバーメッセージをリクエストに保持する
			//request.setAttribute("user", session.getAttribute("navbarMessage"));
			//session.removeAttribute("navbarMessage");

			// 法人一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-corporation-user.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			//logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
