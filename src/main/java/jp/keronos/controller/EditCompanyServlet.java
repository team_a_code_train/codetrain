package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CompanyDao;
import jp.keronos.dto.CompanyDto;
import jp.keronos.sub.CompanyFieldValidator;
import jp.keronos.sub.DataSourceManager;

/**
 * Servlet implementation class EditChannelServlet
 */
@WebServlet("/edit-company")
public class EditCompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(EditCompanyServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 法人一覧画面に遷移する
		response.sendRedirect("list-company.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		//セッションにログインしているユーザー情報が」入っていない場合、list-operator.jspに遷移する
		if (session == null || session.getAttribute("operator") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());

			// 法人一覧画面に遷移する
			request.getRequestDispatcher("list-company.jsp").forward(request, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		CompanyDto dto = new CompanyDto();
		dto.setCompanyId(Integer.parseInt(request.getParameter("companyId")));
		dto.setCompanyName(request.getParameter("companyName"));
		dto.setEmail(request.getParameter("email"));
		// 入力チェック
		List<String> errorMessageList = CompanyFieldValidator.companyValidation(dto);
		if (errorMessageList.size() != 0) {

			// 法人編集画面に遷移する
			request.setAttribute("errorMessageList", errorMessageList);
			request.setAttribute("data", dto);
			request.getRequestDispatcher("WEB-INF/view-company.jsp").forward(request, response);
			return;
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 法人を更新する
			CompanyDao dao = new CompanyDao(conn);
			dao.update(dto);

			// 氏名をリクエストスコープに保持する
			session.setAttribute("message", dto.getCompanyName() + "を更新しました");

			// 法人一覧画面に遷移する
			response.sendRedirect("list-company");

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// 法人名が重複している場合
			if (e.getMessage().contains("Duplicate entry")) {

				// エラーメッセージをリクエストスコープに保持する
				errorMessageList.add("企業名「" + dto.getCompanyName() + "」は既に存在します");
				request.setAttribute("errorMessageList", errorMessageList);

				// フォームのデータをリクエストスコープに保持する
				request.setAttribute("data", dto);

				//法人編集画面に遷移する
				request.getRequestDispatcher("WEB-INF/view-company.jsp").forward(request, response);
			} else {
				// システムエラー画面に遷移する
				request.getRequestDispatcher("system-error.jsp").forward(request, response);
			}
		}
	}
}

