package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.UserDao;
import jp.keronos.dto.UserDto;
import jp.keronos.sub.DataSourceManager;

/**
 * Servlet implementation class ChangePasswordServlet
 */
@WebServlet("/change-password")
public class ChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ChangePasswordServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッションを取得する
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("user") == null) {
			logger.warn("セッションタイムアウトまたは、未ログインアクセス");
			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// エラーメッセージをリクエストに保持する
		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
		session.removeAttribute("errorMessageList");

		// パスワード変更画面に遷移する
		request.getRequestDispatcher("WEB-INF/change-password-skill.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			logger.warn("セッションタイムアウトまたは、未ログインアクセス");
			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// ログインユーザ情報を取得する
		UserDto user = (UserDto) session.getAttribute("user");

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");

		String pass1 = request.getParameter("password1");
		String pass2 = request.getParameter("password2");
		String pass3 = request.getParameter("password3");

		int userId = user.getUserId();

		// スキルの処理 (スキルがnullでない場合)
		String skill = request.getParameter("skill");

		UserDto dto = new UserDto();

		// スキルの処理 (スキルがnullでない場合)
		if (skill != null) {
			dto.setSkill(skill);
			dto.setUserId(userId);
			try (Connection conn = DataSourceManager.getConnection()) {
				// スキルを登録する
				UserDao dao = new UserDao(conn);
				dao.updateSkill(dto);
			} catch (SQLException | NamingException e) {
				logger.error("{} {}", e.getClass(), e.getMessage());

				// システムエラーに遷移する
				response.sendRedirect("system-error.jsp");
				return;
			}
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// パスワードを変更する
			UserDao dao = new UserDao(conn);

			UserDto Dto1 = dao.findByUserIdAndPassword(userId, pass1);
			UserDto Dto2 = dao.findByUserIdAndPassword(userId, pass2);

			// フォームのデータをチェックする
			if (Dto1 == null) {
				session.setAttribute("errorMessage", "現在のパスワードが正しくありません");
				// パスワード変更画面に戻る
				response.sendRedirect("change-password");
				return;
			} else if ("".equals(pass2) || !(pass2).equals(pass3)) {
				session.setAttribute("errorMessage", "新しいパスワードと確認用のパスワードが正しくありません");
				// パスワード変更画面に戻る
				response.sendRedirect("change-password");
				return;
			} else if (Dto2 != null) {
				session.setAttribute("errorMessage", "パスワードが変更されていません");
				// パスワード変更画面に戻る
				response.sendRedirect("change-password");
				return;
			}

			// パスワード変更情報をまとめる
			dto.setPassword(pass2);
			dto.setUserId(user.getUserId());
			// dto.setUpdateUserId(user.getUserId());
			dto.setUpdateNumber(user.getUpdateNumber());

			dao.updatePassword(dto);

			session.setAttribute("message", "パスワードを変更しました");

			// コース一覧画面に遷移する
			response.sendRedirect("list-category");

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラーに遷移する
			response.sendRedirect("system-error.jsp");
		}
	}
}
