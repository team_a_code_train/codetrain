package jp.keronos.controller;

import java.io.IOException;



import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import jp.keronos.dao.OperatorDao;
import jp.keronos.dto.OperatorDto;
import jp.keronos.sub.DataSourceManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




@WebServlet("/list-operator")
public class ListOperatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private Logger logger = LoggerFactory.getLogger(ListOperatorServlet.class);
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");
		
		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			
			if (session == null || session.getAttribute("operator") == null) {
				logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
				// ログイン画面に遷移する
				request.getRequestDispatcher("operator-login").forward(request, response);
				return;
			}

			
			// 運営担当者一覧を取得する
			OperatorDao dao = new OperatorDao(conn);
			List<OperatorDto> list = dao.selectAll();
			
			// 運営担当者一覧データをリクエストに保持する
			request.setAttribute("list", list);
			
			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI());
			
			// メッセージをリクエストに保持する
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");

			// 運営担当者一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-operator.jsp").forward(request, response);
			
		} catch (SQLException | NamingException e) {
			
			logger.error("{} {}", e.getClass(), e.getMessage());
			
			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
