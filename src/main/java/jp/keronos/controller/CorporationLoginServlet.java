package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CorporationUserDao;
import jp.keronos.dto.CorporationUserDto;
import jp.keronos.sub.DataSourceManager;

@WebServlet(urlPatterns={"/corporation-login"}, initParams={@WebInitParam(name="corporationPassword", value="CodeTrain123")})
public class CorporationLoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(CorporationLoginServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");

		// メッセージをリクエストに保持する
		request.setAttribute("Message", session.getAttribute("Message"));
		session.removeAttribute("Message");

		// ログイン画面に遷移する
		request.getRequestDispatcher("WEB-INF/corporation-login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// フォームのデータを取得する
		String corporationLoginId = request.getParameter("corporationLoginId");
		String corporationLoginPassword = request.getParameter("corporationLoginPassword");

		// セッションを取得する
		HttpSession session = request.getSession(true);

		// ログインID、パスワードが未入力の場合
		if ("".equals(corporationLoginId) || "".equals(corporationLoginPassword)) {
			logger.warn("ログイン失敗 {}", request.getRemoteAddr());

			session.setAttribute("Message", "メールアドレス、パスワードを入力してください");
			request.getRequestDispatcher("WEB-INF/corporation-login.jsp").forward(request, response);
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			// ログイン処理
			CorporationUserDao loginDao = new CorporationUserDao(conn);
			CorporationUserDto corporationUserDto =
					loginDao.findByIdAndPassword(corporationLoginId, corporationLoginPassword);

			session.setAttribute("corporationUser", corporationUserDto);

			// ログイン失敗時
			if (corporationUserDto == null) {
				logger.warn("ログイン失敗 {} mail={} pass={}",
					request.getRemoteAddr(), corporationLoginId, corporationLoginPassword);
				session.setAttribute("Message", "メールアドレスまたはパスワードが間違っています");
			}

			// 初回ログイン時はパスワード変更画面にフォワードする
			if (getInitParameter("corporationPassword").equals(corporationLoginPassword)) {
				response.sendRedirect("change-corporation-password");
				return;
			}

			// ログイン成功後は利用者一覧画面に遷移する
			response.sendRedirect("list-user");

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());

			e.printStackTrace();

			// システムエラーに遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}
}
