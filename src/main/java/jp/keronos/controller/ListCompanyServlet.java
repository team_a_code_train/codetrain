package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CompanyDao;
import jp.keronos.dao.UserDao;
import jp.keronos.dto.CompanyDto;
import jp.keronos.dto.UserDto;
import jp.keronos.sub.DataSourceManager;

@WebServlet("/list-company")
public class ListCompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ListCompanyServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(true);

		if (session == null || session.getAttribute("operator") == null) {
			// ログイン画面に遷移する
			request.getRequestDispatcher("operator-login").forward(request, response);
			return;
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			session.removeAttribute("queries");

			//法人一覧を取得する
			CompanyDao dao = new CompanyDao(conn);
			List<CompanyDto> companyList = dao.selectAll();

			// ユーザ数と凍結フラグの数を入れるリストを生成する
			List<UserDto> userCountList = new ArrayList<UserDto>();

			// 履修人数を取得する
			UserDao uDao = new UserDao(conn);
			for(CompanyDto userCountDto : companyList) {
				UserDto userCount
					= uDao.countUserByCompanyId(userCountDto.getCompanyId());

				userCountList.add(userCount);
			}

			//法人一覧データをリクエストに保持する
			request.setAttribute("companyList", companyList);
			request.setAttribute("userCountList", userCountList);

			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI());

			// メッセージをリクエストに保持する
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");

			// 法人一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-company.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
