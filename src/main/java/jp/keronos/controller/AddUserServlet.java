package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.UserDao;
import jp.keronos.dto.CorporationUserDto;
import jp.keronos.dto.UserDto;
import jp.keronos.sub.DataSourceManager;
import jp.keronos.sub.FieldValidator;

@WebServlet("/add-user")
public class AddUserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddUserServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");

		if (session == null || session.getAttribute("corporationUser") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// ログイン画面に遷移する
			request.getRequestDispatcher("corporation-login").forward(request, response);
			return;
		}

		// メッセージをリクエストに保持する
		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
		session.removeAttribute("errorMessageList");

		// 法人利用者追加画面に遷移
		request.getRequestDispatcher("WEB-INF/add-user.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");

		if (session == null || session.getAttribute("corporationUser") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// ログイン画面に遷移する
			request.getRequestDispatcher("corporation-login").forward(request, response);
			return;
		}

		// フォームの全データを取得する
		request.setCharacterEncoding("UTF-8");
		UserDto dto = new UserDto();
		dto.setEmail(request.getParameter("email"));
		dto.setFirstName(request.getParameter("firstName"));
		dto.setLastName(request.getParameter("lastName"));

		// ログインしている法人マスタアカウントの情報を取得する
		CorporationUserDto corporationUser = (CorporationUserDto)session.getAttribute("corporationUser");
		dto.setCompanyId(corporationUser.getCompanyId());
		dto.setCompanyName(corporationUser.getCompanyName());

		// 入力チェック
		List<String> errorMessageList = FieldValidator.userValidation(dto);
		if (errorMessageList.size() != 0) {

			// エラーメッセージをセッションスコープに保持する
			session.setAttribute("errorMessageList", errorMessageList);

			// 法人利用者登録画面に遷移する
			response.sendRedirect("add-user");
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			// 法人利用者を追加する
			UserDao dao = new UserDao(conn);
			dao.insert(dto);

			// 利用者を追加した旨を表示する
			session.setAttribute("message", "利用者を追加しました");

			// 利用者一覧画面に遷移する
			response.sendRedirect("list-user");

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}

}
