package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CourseListDao;
import jp.keronos.dao.HistoryDao;
import jp.keronos.dao.ListCurriculumDao;
import jp.keronos.dto.CourseDto;
import jp.keronos.dto.UserDto;
import jp.keronos.sub.DataSourceManager;

/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/list-course")
public class ListCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ListCourseServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// カテゴリIDを取得する
		int categoryId = Integer.parseInt(request.getParameter("categoryId"));

		// コネクションを取得します
		try (Connection conn = DataSourceManager.getConnection()) {
			HttpSession session = request.getSession(false);

			// 3つのテーブル（カテゴリ表とコース表とカリキュラム表）のDAOを生成
			CourseListDao courseDao = new CourseListDao(conn);
			List<CourseDto> courseList = courseDao.selectByCategoryId(categoryId);
			ListCurriculumDao curriculumDao = new ListCurriculumDao(conn);
			//CategoryDao categorydao = new CategoryDao(conn);

			//カテゴリー一覧情報を取得
			//List<CategoryDto> categoryList = categorydao.selectAll();

			//Map<Integer, List<CourseDto>> courseByCategoryIdList = new HashMap<Integer, List<CourseDto>>();

			// コース一覧情報を取得
			//for(CategoryDto categoryDto : categoryList) {
				//int categoryId = categoryDto.getCategoryId();
			//courseByCategoryIdList.put(categoryId, courseListdao.selectByCategoryId(categoryId));
			//}

			// カリキュラム数追加用リストを生成
			List<CourseDto> courseCountList = new ArrayList<>();

			//List<CourseDto> courseList = CourseListdao.selectAll();

			// コースIDに該当するカリキュラム数を取得する
			for (CourseDto courseCountDto : courseList) {
				int courseIdCount = curriculumDao.selectCountByCourseId(courseCountDto.getCourseId());
				// 生成しなおしたリストにカリキュラム数を入れる
				courseCountDto.setCurriculumCount(courseIdCount);
				courseCountList.add(courseCountDto);
			}

			request.setAttribute("courseList", courseList);
			request.setAttribute("courseCountList", courseCountList);
			//request.setAttribute("categoryList", categoryList);
			//request.setAttribute("courseByCategoryIdList", courseByCategoryIdList);


			if(session.getAttribute("user") != null) {

			// ログインユーザ情報を取得する
			UserDto user = (UserDto) session.getAttribute("user");

			// 利用者IDを取得する
			int id = user.getUserId();

			// ユーザIDに該当する受講済みカリキュラムIDの数（履修済み）を取得する
			HistoryDao historyDao = new HistoryDao(conn);

			// 履修済みカリキュラム数追加用リストを生成
			List<Integer> historyCountList = new ArrayList<Integer>();

			// 履修済みカリキュラム数(ユーザIDとカテゴリIDに該当する、「コースID自体の数」)を取得する
			for (CourseDto historyCountDto : courseList) {
				int historyCount = historyDao.selectCountByUserIdAndCategoryId(id, historyCountDto.getCategoryId(), historyCountDto.getCourseId());
				// 生成しなおしたリストに履修済みカリキュラム数を入れる
				historyCountList.add(historyCount);
			}

			// 各種データをリクエストスコープに保持する
			request.setAttribute("user", user);
			request.setAttribute("historyCountList", historyCountList);
			}

			// URIをリクエストスコープに保持する
			request.setAttribute("uri", request.getRequestURI());

			// セッションスコープ内のナブバーメッセージをリクエストスコープに保持し、セッションスコープ内からナブバーメッセージを削除する
			session.setAttribute("navbarmessage", request.getAttribute("navbarmessage"));
			request.removeAttribute("navbarmessage");

			// list-course.jspに転送する
			request.getRequestDispatcher("/WEB-INF/list-course.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			e.printStackTrace();

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}