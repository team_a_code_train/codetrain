package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.UserDao;
import jp.keronos.sub.DataSourceManager;

@WebServlet("/delete-user")
public class DeleteUserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(DeleteUserServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ログイン画面に遷移する
		response.sendRedirect("corporation-login");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		session.removeAttribute("queries");

		if (session == null || session.getAttribute("corporationUser") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// ログイン画面に遷移する
			request.getRequestDispatcher("corporation-login").forward(request, response);
			return;
		}

		// フォームのデータ(ユーザID、、削除フラグ、更新番号)を取得する
		int userId = Integer.parseInt(request.getParameter("userId"));
		int updateNumber = Integer.parseInt(request.getParameter("updateNumber"));

		try (Connection conn = DataSourceManager.getConnection()) {

			// 法人利用者を論理削除する
			UserDao dao = new UserDao(conn);
			dao.deleteByUserId(userId, updateNumber);

			// 利用者を追加した旨を表示する
			session.setAttribute("message", "利用者を削除しました");

			// 利用者一覧画面に遷移する
			request.getRequestDispatcher("list-user").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}


	}

}
