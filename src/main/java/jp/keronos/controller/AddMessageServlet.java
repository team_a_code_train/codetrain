package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.sub.DataSourceManager;
import jp.keronos.sub.MessageUtility;
import jp.keronos.dao.MessageDao;
import jp.keronos.dto.MessageDto;
import jp.keronos.dto.UserDto;


/**
 * Servlet implementation class AddMessageServlet
 */
@WebServlet("/add-message")
public class AddMessageServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddMessageServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// 遷移する
		request.getRequestDispatcher("WEB-INF/list-message.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// チャンネル一覧画面に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		// ログインユーザ情報を取得する
		UserDto user = (UserDto)session.getAttribute("user");
		
		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		MessageDto messageDto = new MessageDto();
		messageDto.setMessage(request.getParameter("message"));
		//messageDto.setMessageCategoryId(Integer.parseInt(request.getParameter("MESSAGE_CATEGORY_ID")));
		int userId = user.getUserId();
		messageDto.setUserId(userId);
		
		// 入力チェック
		List<String> errorMessageList = MessageUtility.validation(messageDto);
		if (errorMessageList.size() != 0) {
			// ナレッジ登録画面に遷移する
			session.setAttribute("errorMessageList", errorMessageList);
			response.sendRedirect("add-message");
			return;
		}
		
		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			
			// メッセー情報を追加する
			MessageDao messageDao = new MessageDao(conn);
			messageDao.insert(messageDto);
			
			//List<MessageDto> messageList = MessageDao.selectByUserId(userId);

			// メッセージリストをリクエストに保持する
			//request.setAttribute("messageList", messageList);
			
			session.setAttribute("message", "送信しました");
			
			// メッセージ一覧画面に遷移する
			//request.getRequestDispatcher("WEB-INF/list-message.jsp").forward(request, response);
			
			// メッセージ一覧画面に遷移する
			request.getRequestDispatcher("list-message?userId=" + user.getUserId()).forward(request, response);
			
		} catch (SQLException | NamingException e) {
			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}		
	}
}

