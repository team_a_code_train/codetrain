package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CorporationUserDao;
import jp.keronos.dto.CorporationUserDto;
import jp.keronos.sub.DataSourceManager;


/**
 * Servlet implementation class ViewCorporationUser
 */
@WebServlet("/view-corporation-user")
public class ViewCorporationUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ViewOperatorServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("operator") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// 運営担当者一覧画面に遷移する
			request.getRequestDispatcher("list-corporation-user.jsp").forward(request, response);
			return;
		}

		// ログインユーザ情報を取得する
		//OperatorDto operator = (OperatorDto)session.getAttribute("operator");

		// フォームのデータを取得する
		CorporationUserDto dto = new CorporationUserDto();
		request.setCharacterEncoding("UTF-8");
		dto.setUserId(Integer.parseInt(request.getParameter("userId")));
		//dto.setOperatorId(operator.getOperatorId());
		//dto.setOperatorListId(dto.getOperatorListId());


		//dto.setUserId(operator.getuserId());

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 企業アカウント情報を取得する
			CorporationUserDao dao = new CorporationUserDao(conn);

			// ログインユーザが管理権限を持つ者の場合
			//if (operator.isOperatorFlg()) {

				// 運営担当者情報を取得する
				dto= dao.selectByCorporationUserId(dto);

			//} else {

				// 運営担当者情報を取得する

				//dto= dao.selectByCorporationUserId(dto);
			// TODO ログインユーザ更新時に、管理者ユーザが先に更新してしまった場合、dtoが取得できない可能性の考慮
			request.setAttribute("data", dto);

			// 運営担当者編集画面に遷移する
			request.getRequestDispatcher("WEB-INF/view-corporation-user.jsp").forward(request, response);

			}catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
