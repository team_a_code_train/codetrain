package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CourseListDao;
import jp.keronos.dao.HistoryDao;
import jp.keronos.dao.ListCurriculumDao;
import jp.keronos.dao.UserDao;
import jp.keronos.dto.CourseDto;
import jp.keronos.dto.UserDto;
import jp.keronos.sub.DataSourceManager;

@WebServlet("/list-user-detail")
public class ListUserDetailServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(ListUserDetailServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");

		if (session == null || session.getAttribute("corporationUser") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// ログイン画面に遷移する
			request.getRequestDispatcher("corporation-login").forward(request, response);
			return;
		}

		// 法人利用者IDを取得する
		String userId = request.getParameter("userId");
		int uId = Integer.parseInt(userId);

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 法人利用者情報を取得する
			UserDao userDao = new UserDao(conn);
			UserDto userDto = userDao.findByUserId(uId);


			//List<HistoryDto> historyCountList = historyDao.selectCountByUserId(uId);

			// 3つのテーブル（コース表とカリキュラム表と学習履歴表）のDAOを生成する
			CourseListDao courseListdao = new CourseListDao(conn);
			ListCurriculumDao curriculumDao = new ListCurriculumDao(conn);
			HistoryDao historyDao = new HistoryDao(conn);

			// コースIDが必要なのでコース一覧情報を取得
			List<CourseDto> courseList = courseListdao.selectAll();

			// カリキュラム数追加用リストを生成
			List<CourseDto> courseCountList = new ArrayList<>();

			// コースIDに該当するカリキュラム数を取得する
			for(CourseDto courseCountDto : courseList) {
				int courseIdCount = curriculumDao.selectCountByCourseId(courseCountDto.getCourseId());
			// 生成しなおしたリストにカリキュラム数を入れる
				courseCountDto.setCurriculumCount(courseIdCount);
				courseCountList.add(courseCountDto);
			}

			// 履修済みカリキュラム数追加用リストを生成
			List<Integer> historyCountList = new ArrayList<Integer>();

			// 履修済みカリキュラム数(ユーザIDとカテゴリIDに該当する、「コースID自体の数」)を取得する
			for(CourseDto historyCountDto : courseList) {
				int historyCount
					= historyDao.selectCountByUserIdAndCategoryId
						(uId, historyCountDto.getCategoryId(), historyCountDto.getCourseId());
				// 生成しなおしたリストに履修済みカリキュラム数を入れる
				historyCountList.add(historyCount);
			}

			// 各種データをリクエストスコープに保持する
			request.setAttribute("userDto", userDto);
			request.setAttribute("courseList", courseList);
			request.setAttribute("courseCountList", courseCountList);
			request.setAttribute("historyCountList", historyCountList);

			// メッセージをリクエストに保持する
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");

			// ナブバーメッセージをリクエストに保持する
			request.setAttribute("user", session.getAttribute("navbarMessage"));
			session.removeAttribute("navbarMessage");

			// 法人利用者一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-user-detail.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			e.printStackTrace();

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
