package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.UserDao;
import jp.keronos.dto.UserDto;
import jp.keronos.sub.DataSourceManager;

@WebServlet("/edit-user")
public class EditUserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(EditUserServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");

		if (session == null || session.getAttribute("corporationUser") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// ログイン画面に遷移する
			request.getRequestDispatcher("corporation-login").forward(request, response);
			return;
		}

		// メッセージをリクエストに保持する
		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
		session.removeAttribute("errorMessageList");

		// フォームのユーザIDを取得する
		int userId = Integer.parseInt(request.getParameter("userId"));

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 法人利用者情報を取得する
			UserDao userDao = new UserDao(conn);
			UserDto userDto = userDao.findByUserId(userId);

			// 法人利用者情報をリクエストスコープに保持する
			request.setAttribute("userDto", userDto);


			// 編集画面に遷移する
			request.getRequestDispatcher("WEB-INF/edit-user.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		request.setCharacterEncoding("utf-8");

		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");

		if (session == null || session.getAttribute("corporationUser") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// ログイン画面に遷移する
			request.getRequestDispatcher("corporation-login").forward(request, response);
			return;
		}

		// フォームの法人利用者IDを取得する
		int userId = Integer.parseInt(request.getParameter("userId"));

		// フォームのデータの入力チェックをする
		if ("".equals(request.getParameter("reason"))) {
			session.setAttribute("errorMessageList", "休止理由が入力されていません");

			response.sendRedirect("edit-user?userId=" + request.getParameter("userId"));
			return;
		}

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 法人利用者情報を取得する
			UserDao userDao = new UserDao(conn);
			UserDto userDto = userDao.findByUserId(userId);

			// 凍結フラグが立っていなければ、凍結理由を入力し、かつ凍結フラグを立てる
			// 凍結フラグが立っていれば、凍結理由を破棄し、かつ凍結フラグを立てない
			if (userDto.getStatus_flg().equals(false)) {

				// フォームのデータ(凍結理由、更新番号)を取得する
				userDto.setReason(request.getParameter("reason"));
				userDto.setUpdateNumber(Integer.parseInt(request.getParameter("updateNumber")));

				session.setAttribute("data", userDto);

				// 法人利用者を凍結する
				userDao.freeze(userDto);

				// 更新通知をセッションスコープに保持する
				session.setAttribute("message", "利用者を休止状態にしました");

				// 法人利用者一覧画面に遷移する
				response.sendRedirect("list-user");

			} else if(userDto.getStatus_flg().equals(true)){

				// 法人利用者を復帰させる
				userDao.revival(userDto);

				// 更新通知をセッションスコープに保持する
				session.setAttribute("message", "利用者の状態を復帰させました");

				// 法人利用者一覧画面に遷移する
				response.sendRedirect("list-user");

			}

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}

	}

}
