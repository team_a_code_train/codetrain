package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CorporationUserDao;
import jp.keronos.dto.CorporationUserDto;
import jp.keronos.sub.DataSourceManager;
import jp.keronos.sub.FieldValidator;


@WebServlet("/add-corporation-user")
public class AddCorporationUserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(AddCorporationUserServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());


		// セッションを取得する
		HttpSession session = request.getSession(true);
		session.removeAttribute("queries");

		if (session == null || session.getAttribute("operator") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());
			// 企業一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-company.jsp").forward(request, response);
			return;
		}

		// フォームのデータを取得する

		try (Connection conn = DataSourceManager.getConnection()) {

			// 企業アカウント一覧を取得する
			CorporationUserDao dao = new CorporationUserDao(conn);
			List<CorporationUserDto> list = dao.selectAll();

			// チャンネル一覧データをリクエストに保持する
			request.setAttribute("list", list);

			// リクエストスコープに追加モードフラグを保持する
			request.setAttribute("isAdd", true);

			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI());

			// メッセージをリクエストに保持する
			request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
			session.removeAttribute("errorMessageList");

			// 企業一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/view-corporation-user.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// セッションを取得する
		HttpSession session = request.getSession(true);

		session.removeAttribute("queries");

		if (session == null || session.getAttribute("operator") == null) {
			logger.warn("セッションタイムアウト {}", request.getRemoteAddr());

			// 企業一覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-company.jsp").forward(request, response);
			return;
		}

		// セッションからログインユーザ情報を取得する
		//OperatorDto operator = (OperatorDto)session.getAttribute("operator");

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		CorporationUserDto dto = new CorporationUserDto();
		dto.setCompanyId(Integer.parseInt(request.getParameter("companyId")));
		dto.setCompanyName(request.getParameter("companyName"));
		dto.setFirstName(request.getParameter("firstName"));
		dto.setLastName(request.getParameter("lastName"));
		dto.setEmail(request.getParameter("email"));

		// 入力チェック
		List<String> errorMessageList = FieldValidator.corporationUserValidation(dto);
		if (errorMessageList.size() != 0) {

			// エラーメッセージをセッションスコープに保持する
			session.setAttribute("errorMessageList", errorMessageList);

			// アカウント登録画面に遷移する
			response.sendRedirect("add-corporation-user");
			return;
		}

		try (Connection conn = DataSourceManager.getConnection()) {

			// 企業アカウントを追加する
			CorporationUserDao dao = new CorporationUserDao(conn);
			dao.insert(dto);

			// アカウントを追加した旨を表示する
			session.setAttribute("message", "追加しました");

			// 企業一覧画面に遷移する
			response.sendRedirect("list-company");

		} catch (SQLException | NamingException e) {


			logger.error("{} {}", e.getClass(), e.getMessage());
			e.printStackTrace();

			if (e.getMessage().contains("Duplicate entry")) {

				// エラーメッセージをリクエストスコープに保持する
				errorMessageList.add("既に存在します");
				request.setAttribute("errorMessageList", errorMessageList);

				// フォームのデータをリクエストスコープに保持する
				request.setAttribute("data", dto);


				// システムエラー画面に遷移する
				request.getRequestDispatcher("WEB-INF/view-corporation-user.jsp").forward(request, response);
			}
		}
	}
}
