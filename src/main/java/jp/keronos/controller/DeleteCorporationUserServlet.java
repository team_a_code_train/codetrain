package jp.keronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.keronos.dao.CorporationUserDao;
import jp.keronos.dto.CorporationUserDto;
import jp.keronos.sub.DataSourceManager;



/**
 * Servlet implementation class DeleteCorporationUserServlet
 */
@WebServlet("/delete-corporation-user")
public class DeleteCorporationUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(DeleteOperatorServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 運用担当者一覧に遷移する
		response.sendRedirect("list-corporation-user.jsp");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());

		// セッションを取得する
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("operator") == null) {

			logger.warn("セッションタイムアウト");
			// 運営担当者一覧画面に遷移する
			request.getRequestDispatcher("list-corporation").forward(request, response);
			return;
		}

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		CorporationUserDto dto = new CorporationUserDto();
		dto.setUserId(Integer.parseInt(request.getParameter("userId")));
		dto.setUpdateNumber(Integer.parseInt(request.getParameter("updateNumber")));

		// コネクションを取得する
		String firstName = "";
		String lastName = "";
		try (Connection conn = DataSourceManager.getConnection()) {

			try {
				// トランザクションを開始する
				conn.setAutoCommit(false);

				// 企業アカウント情報を削除する
				CorporationUserDao corporationUserDao = new CorporationUserDao(conn);
				//operatorDao.deleteByOperatorId(dto.getOperatorId());


				//firstName = corporationUserDao.selectByCorporationUserId(dto).getFirstName();
				//lastName = corporationUserDao.selectByCorporationUserId(dto).getLastName();
				corporationUserDao.deleteByCorporationUserId(dto.getUserId());
				//CorporationUserDao.deleteByCorporationUserId(dto.getOperatorId());

				session.setAttribute("message","企業アカウントを削除しました");

			} catch (SQLException e) {

				logger.error("{} {}", e.getClass(), e.getMessage());

				// ロールバックする
				conn.rollback();
				throw e;
			} finally {
				conn.setAutoCommit(true);
			}

			// 運営担当者一覧画面に遷移する
			response.sendRedirect("list-company");

		} catch (SQLException | NamingException e) {

			logger.error("{} {}", e.getClass(), e.getMessage());

			// システムエラー画面に遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);

		}
	}
}
